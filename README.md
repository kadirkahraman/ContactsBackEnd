### Contacts BackEnd application

##### Used Technologies
* Spring JDBC & JPA
* Spring Security
* Liquibase
* JWT
* API Tests
* Unit Tests
* Integration & Data JPA Tests 

-----------------------------------------------------------------

##### Compile options

| Option | Command |
| ------ | ------ |
| compile | mvn clean compile |
| run | mvn springboot:run |
| test | mvn clean test |
| package | mvn clean package -DskipTests=true | 
