package com.kahraman.contacts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(scanBasePackages = {"com.kahraman.contacts"})
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {
        "com.kahraman.contacts.user",
        "com.kahraman.contacts.userrole",
        "com.kahraman.contacts.contact",
})
@EntityScan(basePackages = {
        "com.kahraman.contacts.user",
        "com.kahraman.contacts.userrole",
        "com.kahraman.contacts.contact",
})
@ComponentScan(basePackages = {
        "com.kahraman.contacts",
        "com.kahraman.contacts.authentication",
        "com.kahraman.contacts.contact",
        "com.kahraman.contacts.restapi",
        "com.kahraman.contacts.springsecurity",
        "com.kahraman.contacts.controller",
        "com.kahraman.contacts.user",})
public class ContactsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ContactsApplication.class, args);
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}