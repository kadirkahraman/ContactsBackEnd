package com.kahraman.contacts.authentication;

import com.kahraman.contacts.user.UserDto;

public class AuthResult {

    private UserDto authenticatedUser;
    private String token;
    private TokenDetails tokenDetails;

    AuthResult() {
    }

    public UserDto getAuthenticatedUser() {
        return authenticatedUser;
    }

    void setAuthenticatedUser(UserDto authenticatedUser) {
        this.authenticatedUser = authenticatedUser;
    }

    public String getToken() {
        return token;
    }

    void setToken(String token) {
        this.token = token;
    }

    public TokenDetails getTokenDetails() {
        return tokenDetails;
    }

    void setTokenDetails(TokenDetails tokenDetails) {
        this.tokenDetails = tokenDetails;
    }
}