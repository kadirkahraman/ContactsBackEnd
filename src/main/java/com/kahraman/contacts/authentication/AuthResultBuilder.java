package com.kahraman.contacts.authentication;

import com.kahraman.contacts.exceptions.BuilderException;
import com.kahraman.contacts.user.User;
import com.kahraman.contacts.user.UserDto;
import org.apache.commons.lang3.StringUtils;

public class AuthResultBuilder {

    private final UserDto authenticatedUser;
    private String token;
    private JwtTokenUtil tokenUtil;

    public AuthResultBuilder(User authenticatedUser) {
        this(UserDto.of(authenticatedUser));
    }

    public AuthResultBuilder(UserDto authenticatedUser) {
        this.authenticatedUser = authenticatedUser;
    }

    public AuthResultBuilder token(String token) {
        this.token = token;
        return this;
    }

    public AuthResultBuilder tokenDetails(JwtTokenUtil tokenUtil) {
        this.tokenUtil = tokenUtil;
        return this;
    }

    public AuthResult build() {
        if (authenticatedUser == null) {
            throw new BuilderException("User couldn't be null");
        }
        if (StringUtils.isEmpty(token)) {
            throw new BuilderException("Token couldn't be empty");
        }

        AuthResult authResult = new AuthResult();
        authResult.setTokenDetails(new TokenDetails(token, tokenUtil));
        authResult.setToken(token);
        authResult.setAuthenticatedUser(authenticatedUser);
        return authResult;
    }
}
