package com.kahraman.contacts.authentication;

import com.kahraman.contacts.exceptions.ServiceException;
import com.kahraman.contacts.springsecurity.JwtUserDetailsService;
import com.kahraman.contacts.springsecurity.SecurityPrincipal;
import com.kahraman.contacts.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthenticationService {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtUserDetailsService userDetailsService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    private void authenticate(String username, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new ServiceException("User was disabled!", e);
        } catch (BadCredentialsException e) {
            throw new ServiceException("Credentials are not valid!", e);
        }
    }

    public AuthResult loginAndGenerateToken(String username, String password) {
        authenticate(username, password);

        final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        String token = jwtTokenUtil.generateToken(userDetails);

        return new AuthResultBuilder(((SecurityPrincipal) userDetails).getUser())
                .token(token)
                .tokenDetails(jwtTokenUtil)
                .build();
    }

    public AuthResult prepareUserDetails(String token) {
        User currentUser = ((SecurityPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        return new AuthResultBuilder(currentUser)
                .token(token)
                .tokenDetails(jwtTokenUtil)
                .build();
    }

    public Optional<User> authenticatedUser() {
        User currentUser = ((SecurityPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        return Optional.ofNullable(currentUser);
    }
}
