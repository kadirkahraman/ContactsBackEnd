package com.kahraman.contacts.authentication;

import org.springframework.lang.Nullable;

import java.util.Date;
import java.util.Optional;

public class TokenDetails {

    private final String username;
    private final Date expireDate;
    private final Boolean tokenExpired;

    TokenDetails(String token, @Nullable JwtTokenUtil jwtTokenUtil) {
        this.expireDate = Optional.ofNullable(jwtTokenUtil).map(tokenUtil -> tokenUtil.getExpirationDateFromToken(token)).orElse(null);
        this.username = Optional.ofNullable(jwtTokenUtil).map(tokenUtil -> tokenUtil.getUsernameFromToken(token)).orElse(null);
        this.tokenExpired = Optional.ofNullable(jwtTokenUtil).map(tokenUtil -> tokenUtil.isTokenExpired(token)).orElse(null);
    }

    public String getUsername() {
        return username;
    }

    public Long getExpireDate() {
        return expireDate.getTime();
    }

    public Boolean isTokenExpired() {
        return tokenExpired;
    }
}
