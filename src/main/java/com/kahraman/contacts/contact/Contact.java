package com.kahraman.contacts.contact;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "contact")
public class Contact {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "contactSequenceGenerator")
    @GenericGenerator(name = "contactSequenceGenerator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "sequence_name", value = "HIBERNATE_SEQUENCE")
            })
    private Long id;

    @Column(name = "f_name")
    private String firstName;

    @Column(name = "l_name")
    private String lastName;

    @Column(name = "nick_name")
    private String nickName;

    @Column(name = "city")
    private String city;

    @Column(name = "birth_day")
    private Date birthDay;

    @Column(name = "phone")
    private String phone;

    @Column(name = "alternative_phone")
    private String phoneAlternative;

    @Column(name = "mail")
    private String mail;

    @Column(name = "alternative_mail")
    private String mailAlternative;

    @Column(name = "fav")
    private Boolean favourite;

    Contact() {
    }

    public Long getId() {
        return id;
    }

    void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickName() {
        return nickName;
    }

    void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getCity() {
        return city;
    }

    void setCity(String city) {
        this.city = city;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public String getPhone() {
        return phone;
    }

    void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneAlternative() {
        return phoneAlternative;
    }

    void setPhoneAlternative(String phoneAlternative) {
        this.phoneAlternative = phoneAlternative;
    }

    public String getMail() {
        return mail;
    }

    void setMail(String mail) {
        this.mail = mail;
    }

    public String getMailAlternative() {
        return mailAlternative;
    }

    void setMailAlternative(String mailAlternative) {
        this.mailAlternative = mailAlternative;
    }

    public Boolean getFavourite() {
        return favourite;
    }

    void setFavourite(Boolean favourite) {
        this.favourite = favourite;
    }
}
