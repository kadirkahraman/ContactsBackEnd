package com.kahraman.contacts.contact;

import com.kahraman.contacts.exceptions.BuilderException;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

public class ContactBuilder {

    private Contact contact;

    public ContactBuilder() {
        this.contact = new Contact();
    }

    public ContactBuilder withId(Long id) {
        this.contact.setId(id);
        return this;
    }

    public ContactBuilder withFirstName(String firstName) {
        this.contact.setFirstName(firstName);
        return this;
    }

    public ContactBuilder withLastName(String lastName) {
        this.contact.setLastName(lastName);
        return this;
    }

    public ContactBuilder withNickName(String nickName) {
        this.contact.setNickName(nickName);
        return this;
    }

    public ContactBuilder withCity(String city) {
        this.contact.setCity(city);
        return this;
    }

    public ContactBuilder withBirthDay(Date birthDay) {
        this.contact.setBirthDay(birthDay);
        return this;
    }

    public ContactBuilder withPhone(String phone) {
        this.contact.setPhone(phone);
        return this;
    }

    public ContactBuilder withPhoneAlternative(String phoneAlternative) {
        this.contact.setPhoneAlternative(phoneAlternative);
        return this;
    }

    public ContactBuilder withMail(String mail) {
        this.contact.setMail(mail);
        return this;
    }

    public ContactBuilder withMailAlternative(String mailAlternative) {
        this.contact.setMailAlternative(mailAlternative);
        return this;
    }

    public ContactBuilder withFavourite(Boolean favourite) {
        this.contact.setFavourite(favourite);
        return this;
    }

    public Contact build() {
        if (StringUtils.isEmpty(this.contact.getFirstName())) {
            throw new BuilderException("Contact first name couldn't be empty.");
        }
        if (StringUtils.isEmpty(this.contact.getLastName())) {
            throw new BuilderException("Contact last name couldn't be empty.");
        }
        if (StringUtils.isEmpty(this.contact.getPhone()) && StringUtils.isEmpty(this.contact.getPhoneAlternative())) {
            throw new BuilderException("Contact phone couldn't be empty.");
        }

        return this.contact;
    }
}
