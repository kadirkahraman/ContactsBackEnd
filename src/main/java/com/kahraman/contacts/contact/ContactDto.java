package com.kahraman.contacts.contact;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.Date;

public class ContactDto {

    private final Long id;
    private final String firstName;
    private final String lastName;
    private final String nickName;
    private final String city;
    private final Date birthDay;
    private final String phone;
    private final String phoneAlternative;
    private final String mail;
    private final String mailAlternative;
    private final Boolean favourite;

    private ContactDto(@NonNull Contact contact) {
        this.id = contact.getId();
        this.firstName = contact.getFirstName();
        this.lastName = contact.getLastName();
        this.nickName = contact.getNickName();
        this.city = contact.getCity();
        this.birthDay = contact.getBirthDay();
        this.phone = contact.getPhone();
        this.phoneAlternative = contact.getPhoneAlternative();
        this.mail = contact.getMail();
        this.mailAlternative = contact.getMailAlternative();
        this.favourite = contact.getFavourite();
    }

    public static ContactDto of(@Nullable Contact contact) {
        if (contact == null) {
            return null;
        }
        return new ContactDto(contact);
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getNickName() {
        return nickName;
    }

    public String getCity() {
        return city;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public String getPhone() {
        return phone;
    }

    public String getPhoneAlternative() {
        return phoneAlternative;
    }

    public String getMail() {
        return mail;
    }

    public String getMailAlternative() {
        return mailAlternative;
    }

    public Boolean getFavourite() {
        return favourite;
    }
}
