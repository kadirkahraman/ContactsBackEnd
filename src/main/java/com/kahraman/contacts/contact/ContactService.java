package com.kahraman.contacts.contact;

import com.kahraman.contacts.authentication.AuthenticationService;
import com.kahraman.contacts.exceptions.ServiceException;
import com.kahraman.contacts.user.User;
import com.kahraman.contacts.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class ContactService {
    private static final Logger logger = LoggerFactory.getLogger(ContactService.class);

    @Autowired
    private ContactRepository contactRepository;
    @Autowired
    private AuthenticationService authenticationService;
    @Autowired
    private UserService userService;

    public ContactDto create(Contact contact) {
        if (contact.getId() != null) {
            throw new ServiceException("Contact id must be null on create process");
        }
        logger.info("Contact with name {} {} is preparing to create", contact.getLastName(), contact.getFirstName());
        return save(contact);
    }

    public ContactDto update(Contact contact) {
        if (contact.getId() == null) {
            throw new ServiceException("contact id required for update process");
        }
        logger.info("Contact with name {} {} (#{}) is preparing to update", contact.getLastName(), contact.getFirstName(), contact.getId());
        return save(contact);
    }

    @Transactional
    public ContactDto save(Contact contact) {
        Contact savedContact = contactRepository.save(contact);
        logger.info("Contact with name {} {} (#{}) saved", contact.getLastName(), contact.getFirstName(), contact.getId());
        User authenticatedUser = authenticationService.authenticatedUser().orElseThrow(() -> new ServiceException("Authentication couldn't found"));
        userService.addContact(authenticatedUser, savedContact);
        return ContactDto.of(savedContact);
    }

    public Long delete(Long id) {
        Optional<Contact> contact = contactRepository.findById(id);
        if (!contact.isPresent()) {
            throw new ServiceException("Contact with id %s couldn't found", id);
        }
        return delete(contact.get());
    }

    public ContactDto findContact(Long id) {
        Optional<Contact> contact = contactRepository.findById(id);
        return ContactDto.of(contact.orElse(null));
    }

    @Transactional
    public Long delete(Contact contact) {
        User currentUser = authenticationService.authenticatedUser().orElseThrow(() -> new ServiceException("Authentication couldn't found"));
        userService.removeContact(currentUser, contact);
        contactRepository.delete(contact);
        return contact.getId();
    }
}
