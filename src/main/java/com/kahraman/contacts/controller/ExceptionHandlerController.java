package com.kahraman.contacts.controller;

import com.kahraman.contacts.exceptions.BuilderException;
import com.kahraman.contacts.exceptions.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * @author kadir
 */
@Controller
@ControllerAdvice
class ExceptionHandlerController {

    private static final Logger logger = LoggerFactory.getLogger(ExceptionHandlerController.class);

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    @ResponseBody
    String unexpectedErrorHandler(Exception e) {
        logger.error(e.getMessage(), e);
        return Optional.ofNullable(e.getMessage()).orElse("Beklenmedik bir hata tespit edildi");
    }

    @ExceptionHandler(value = {
            ServiceException.class,
            BuilderException.class,
            RuntimeException.class
    })
    @ResponseBody
    ResponseEntity<String> runtimeExceptionHandler(Exception e) {
        logger.error(e.getMessage(), e);
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .contentType(MediaType.TEXT_PLAIN)
                .body(e.getMessage());
    }

    @RequestMapping("/403")
    @ExceptionHandler(BadCredentialsException.class)
    ResponseEntity<String> forbidden(HttpServletRequest request) {
        logger.error(HttpStatus.FORBIDDEN.name());
        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .contentType(MediaType.TEXT_PLAIN)
                .body("Bu işlemi yapmak için izniniz yok");
    }

    @RequestMapping("/404")
    @ExceptionHandler(NoHandlerFoundException.class)
    ResponseEntity<String> notFound(HttpServletRequest request) {
        logger.error(HttpStatus.NOT_FOUND.name());
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .contentType(MediaType.TEXT_PLAIN)
                .body("Böyle bir alan bulunamadı");
    }
}