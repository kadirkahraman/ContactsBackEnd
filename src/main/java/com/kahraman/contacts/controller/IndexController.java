package com.kahraman.contacts.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class IndexController {

    @GetMapping(path = {"/"})
    public String index() {
        return "Contacts back end is ready";
    }
}