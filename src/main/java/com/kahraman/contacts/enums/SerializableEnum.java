package com.kahraman.contacts.enums;

public interface SerializableEnum {

    /**
     * Serialize enum name for UI
     */
    String getEnumName();

}
