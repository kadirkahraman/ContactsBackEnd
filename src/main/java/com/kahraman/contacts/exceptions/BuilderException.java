package com.kahraman.contacts.exceptions;

public class BuilderException extends RuntimeException {

    public BuilderException(String message, Object... args) {
        super(String.format(message, args));
    }
}
