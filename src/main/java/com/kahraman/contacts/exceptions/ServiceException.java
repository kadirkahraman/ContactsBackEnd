package com.kahraman.contacts.exceptions;

public class ServiceException extends RuntimeException {

    public ServiceException(String message, Object... args) {
        super(String.format(message, args));
    }

    public ServiceException(Throwable throwable, String message, Object... args) {
        super(String.format(message, args), throwable);
    }
}
