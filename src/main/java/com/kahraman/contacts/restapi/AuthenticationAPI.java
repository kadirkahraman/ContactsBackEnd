package com.kahraman.contacts.restapi;

import com.kahraman.contacts.authentication.AuthResult;
import com.kahraman.contacts.authentication.AuthenticationService;
import com.kahraman.contacts.exceptions.ServiceException;
import com.kahraman.contacts.user.User;
import com.kahraman.contacts.user.UserDto;
import com.kahraman.contacts.user.UserService;
import com.kahraman.contacts.userrole.UserRole;
import com.kahraman.contacts.userrole.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static com.kahraman.contacts.userrole.PredefinedRole.STANDARD_USER;
import static com.kahraman.contacts.userrole.PredefinedRole.SYSTEM_ADMIN;

@RestController
@CrossOrigin
@RequestMapping(path = "api/auth")
class AuthenticationAPI {

    @Autowired
    private AuthenticationService authenticationService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserRoleService userRoleService;

    @PostMapping(value = "/login")
    AuthResult login(@RequestBody LoginModel authenticationRequest) {
        return authenticationService.loginAndGenerateToken(authenticationRequest.getUsername(), authenticationRequest.getPassword());
    }

    @PostMapping("/signUp")
    UserDto signUp(UserCreateModel model) {
        UserRole standardRole = userRoleService.findUserRoleByName(STANDARD_USER.getName()).orElseThrow(() -> new ServiceException("User Role with name %s couldn't found", STANDARD_USER.getName()));
        User user = model.toBuilder().userRole(standardRole).build();
        if (userService.userCount() == 0L) {
            UserRole systemAdminRole = userRoleService.findUserRoleByName(SYSTEM_ADMIN.getName()).orElseThrow(() -> new ServiceException("User Role with name %s couldn't found", SYSTEM_ADMIN.getName()));
            user = model.toBuilder().userRole(systemAdminRole).build();
        }
        return userService.create(user);
    }

    @PostMapping("/updatePassword")
    UserDto updatePassword(@RequestParam String oldPass, @RequestParam String newPass) {
        Optional<User> currentUser = authenticationService.authenticatedUser();
        if (!currentUser.isPresent()) {
            throw new ServiceException("Authentication couldn't found");
        }
        return userService.updatePassword(currentUser.get(), oldPass, newPass);
    }

    @PostMapping("/me")
    AuthResult authenticatedUser(@RequestParam String token) {
        return authenticationService.prepareUserDetails(token);
    }
}