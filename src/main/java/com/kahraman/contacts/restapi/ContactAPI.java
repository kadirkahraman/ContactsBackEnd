package com.kahraman.contacts.restapi;

import com.kahraman.contacts.contact.ContactDto;
import com.kahraman.contacts.contact.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api/contact")
class ContactAPI {

    @Autowired
    private ContactService contactService;

    @GetMapping(path = "/{id}")
    @Secured(value = {"ROLE_CONTACT_ADMIN"})
    ContactDto findContact(@PathVariable Long id) {
        return contactService.findContact(id);
    }

    @PostMapping
    @Secured(value = {"ROLE_CONTACT_ADMIN"})
    ContactDto create(ContactCreateModel contactCreateModel) {
        return contactService.create(contactCreateModel.toBuilder().build());
    }

    @PutMapping
    @Secured(value = {"ROLE_CONTACT_ADMIN"})
    ContactDto update(ContactUpdateModel contactUpdateModel) {
        return contactService.update(contactUpdateModel.toBuilder().build());
    }

    @DeleteMapping(path = "/{id}")
    Long delete(@PathVariable Long id) {
        return contactService.delete(id);
    }
}
