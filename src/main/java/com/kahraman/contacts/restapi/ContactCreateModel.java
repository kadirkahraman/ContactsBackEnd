package com.kahraman.contacts.restapi;

import com.kahraman.contacts.contact.ContactBuilder;

import java.util.Date;

class ContactCreateModel {

    private String firstName;
    private String lastName;
    private String nickName;
    private String city;
    private Date birthDay;
    private String phone;
    private String phoneAlternative;
    private String mail;
    private String mailAlternative;
    private Boolean favourite;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneAlternative() {
        return phoneAlternative;
    }

    public void setPhoneAlternative(String phoneAlternative) {
        this.phoneAlternative = phoneAlternative;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMailAlternative() {
        return mailAlternative;
    }

    public void setMailAlternative(String mailAlternative) {
        this.mailAlternative = mailAlternative;
    }

    public Boolean getFavourite() {
        return favourite;
    }

    public void setFavourite(Boolean favourite) {
        this.favourite = favourite;
    }

    ContactBuilder toBuilder() {
        return new ContactBuilder()
                .withPhone(phone)
                .withLastName(lastName)
                .withFirstName(firstName)
                .withFavourite(favourite)
                .withMail(mail)
                .withMailAlternative(mailAlternative)
                .withPhoneAlternative(phoneAlternative)
                .withBirthDay(birthDay)
                .withCity(city)
                .withNickName(nickName);
    }
}
