package com.kahraman.contacts.restapi;

import com.kahraman.contacts.contact.ContactBuilder;

class ContactUpdateModel extends ContactCreateModel {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    ContactBuilder toBuilder() {
        return super.toBuilder().withId(id);
    }
}
