package com.kahraman.contacts.restapi;

import com.kahraman.contacts.user.UserDto;
import com.kahraman.contacts.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@CrossOrigin
@RestController
@RequestMapping(path = "api/user")
class UserAPI {

    @Autowired
    private UserService userService;

    @GetMapping
    @Secured(value = {"ROLE_USER_ADMIN", "ROLE_USER_VIEW"})
    Set<UserDto> list() {
        return userService.allUsers();
    }

    @PostMapping
    @Secured(value = {"ROLE_USER_ADMIN"})
    UserDto create(UserCreateModel model) {
        return userService.create(model.toBuilder().build());
    }

    @PutMapping
    @Secured(value = {"ROLE_USER_ADMIN"})
    UserDto update(UserUpdateModel updateModel) {
        return userService.update(updateModel.toBuilder().build());
    }

    @DeleteMapping(path = "/{id}")
    @Secured(value = {"ROLE_USER_ADMIN"})
    Long delete(@PathVariable Long id) {
        return userService.delete(id);
    }
}
