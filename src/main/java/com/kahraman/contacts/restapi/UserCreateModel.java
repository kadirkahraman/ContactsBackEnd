package com.kahraman.contacts.restapi;

import com.kahraman.contacts.contact.Contact;
import com.kahraman.contacts.user.UserBuilder;
import com.kahraman.contacts.userrole.UserRole;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.Set;

class UserCreateModel {

    private String email;
    private String fullName;
    private String username;
    private UserRole userRole;
    private String password;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date birthDay;
    private Set<Contact> contacts;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public Set<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Set<Contact> contacts) {
        this.contacts = contacts;
    }

    public UserBuilder toBuilder() {
        return new UserBuilder()
                .email(email)
                .fullName(fullName)
                .username(username)
                .userRole(userRole)
                .password(password)
                .birthDay(birthDay)
                .contacts(contacts);
    }
}
