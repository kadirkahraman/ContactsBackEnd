package com.kahraman.contacts.restapi;

import com.kahraman.contacts.userrole.Permission;
import com.kahraman.contacts.userrole.UserRoleDto;
import com.kahraman.contacts.userrole.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(path = "api/userrole")
class UserRoleAPI {

    @Autowired
    private UserRoleService userRoleService;

    @GetMapping(path = "/permissions")
    @Secured(value = {"ROLE_USER_ADMIN", "ROLE_USER_VIEW"})
    Permission[] permissions() {
        return Permission.values();
    }

    @GetMapping
    @Secured(value = {"ROLE_USER_ADMIN", "ROLE_USER_VIEW"})
    List<UserRoleDto> list() {
        return userRoleService.list();
    }

    @PostMapping
    @Secured(value = {"ROLE_USER_ADMIN"})
    UserRoleDto create(UserRoleCreateModel model) {
        return userRoleService.create(model.toBuilder().build());
    }

    @PutMapping
    @Secured(value = {"ROLE_USER_ADMIN"})
    UserRoleDto update(UserRoleUpdateModel updateModel) {
        return userRoleService.update(updateModel.toBuilder().build());
    }

    @DeleteMapping(path = "/{id}")
    @Secured(value = {"ROLE_USER_ADMIN"})
    Long delete(@PathVariable Long id) {
        return userRoleService.delete(id);
    }
}
