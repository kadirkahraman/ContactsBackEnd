package com.kahraman.contacts.restapi;

import com.kahraman.contacts.userrole.Permission;
import com.kahraman.contacts.userrole.UserRoleBuilder;

import java.util.Set;

public class UserRoleCreateModel {

    private String name;
    private String description;
    private Set<Permission> permissions;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    public UserRoleBuilder toBuilder() {
        return new UserRoleBuilder()
                .permissions(getPermissions())
                .name(getName())
                .description(getDescription());
    }
}
