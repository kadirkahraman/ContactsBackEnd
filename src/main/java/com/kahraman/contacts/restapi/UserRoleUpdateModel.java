package com.kahraman.contacts.restapi;

import com.kahraman.contacts.userrole.UserRoleBuilder;

public class UserRoleUpdateModel extends UserRoleCreateModel {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public UserRoleBuilder toBuilder() {
        return super.toBuilder().id(id);
    }
}
