package com.kahraman.contacts.restapi;

import com.kahraman.contacts.user.UserBuilder;

public class UserUpdateModel extends UserCreateModel {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public UserBuilder toBuilder() {
        return super.toBuilder()
                .id(id);
    }
}
