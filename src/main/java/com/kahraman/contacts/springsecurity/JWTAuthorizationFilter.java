package com.kahraman.contacts.springsecurity;

import io.jsonwebtoken.Jwts;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private final String tokenPrefix;
    private final String headerKey;
    private final String secretKey;
    private final JwtUserDetailsService jwtUserDetailsService;

    JWTAuthorizationFilter(AuthenticationManager authManager, String tokenPrefix, String headerKey, String secretKey, JwtUserDetailsService jwtUserDetailsService) {
        super(authManager);
        this.tokenPrefix = tokenPrefix;
        this.headerKey = headerKey;
        this.secretKey = secretKey;
        this.jwtUserDetailsService = jwtUserDetailsService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader(headerKey);

        if (header == null || !header.startsWith(tokenPrefix)) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(req);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String tokenWithPrefix = request.getHeader(headerKey);
        try {
            String user = Jwts.parser()
                    .setSigningKey(secretKey)
                    .parseClaimsJws(extractToken(tokenWithPrefix))
                    .getBody()
                    .getSubject();

            if (user != null) {
                SecurityPrincipal principal = (SecurityPrincipal) jwtUserDetailsService.loadUserByUsername(user);
                return new UsernamePasswordAuthenticationToken(principal, principal.getPassword(), principal.getAuthorities());
            }
        } catch (Exception e) {
            throw new AccessDeniedException(e.getMessage(), e);
        }
        throw new AccessDeniedException("Authentication token was not found!");
    }

    private String extractToken(String completeToken) {
        return completeToken.replace(String.format("%s ", tokenPrefix), "");
    }
}