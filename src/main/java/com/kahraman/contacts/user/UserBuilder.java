package com.kahraman.contacts.user;

import com.kahraman.contacts.contact.Contact;
import com.kahraman.contacts.exceptions.BuilderException;
import com.kahraman.contacts.userrole.UserRole;
import com.kahraman.contacts.utils.ValidationUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.Set;

public class UserBuilder {

    private final User user;

    public UserBuilder() {
        this.user = new User();
    }

    public UserBuilder id(Long id) {
        this.user.setId(id);
        return this;
    }

    public UserBuilder email(String email) {
        this.user.setEmail(email);
        return this;
    }

    public UserBuilder fullName(String fullName) {
        this.user.setFullName(fullName);
        return this;
    }

    public UserBuilder username(String username) {
        this.user.setUsername(username);
        return this;
    }

    public UserBuilder userRole(UserRole userRole) {
        this.user.setUserRole(userRole);
        return this;
    }

    public UserBuilder password(String password) {
        this.user.setPassword(password);
        return this;
    }

    public UserBuilder birthDay(Date birthDay) {
        this.user.setBirthDay(birthDay);
        return this;
    }

    public UserBuilder contacts(Set<Contact> contacts) {
        this.user.setContacts(contacts);
        return this;
    }

    public UserBuilder addContacts(Contact... contacts) {
        Set<Contact> contactSet = this.user.getContacts();
        contactSet.addAll(Arrays.asList(contacts));
        return this;
    }

    public User build() {
        if (!ValidationUtils.validateMail(this.user.getEmail())) {
            throw new BuilderException("Email format is not valid");
        }
        if (StringUtils.isEmpty(this.user.getFullName())) {
            throw new BuilderException("Name is required parameter");

        }
        if (StringUtils.isEmpty(this.user.getUsername())) {
            throw new BuilderException("Username is required parameter");

        }
        if (this.user.getUserRole() == null) {
            throw new BuilderException("User role is required parameter");

        }
        if (this.user.getId() == null && StringUtils.isEmpty(this.user.getPassword())) {
            throw new BuilderException("Password is required parameter");
        }
        return this.user;
    }
}
