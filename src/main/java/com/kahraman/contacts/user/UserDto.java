package com.kahraman.contacts.user;

import com.kahraman.contacts.contact.ContactDto;
import com.kahraman.contacts.userrole.UserRoleDto;

import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

public class UserDto {

    private final Long id;
    private final String email;
    private final String fullName;
    private final String username;
    private final UserRoleDto userRole;
    private final Date birthDay;
    private final Set<ContactDto> contacts;

    public UserDto(Long id, String email, String fullName, String username, UserRoleDto userRole, Date birthDay, Set<ContactDto> contacts) {
        this.id = id;
        this.email = email;
        this.fullName = fullName;
        this.username = username;
        this.userRole = userRole;
        this.birthDay = birthDay;
        this.contacts = contacts;
    }

    public static UserDto of(User user) {
        if (user == null) {
            return null;
        }
        Set<ContactDto> contacts = user.getContacts()
                .stream()
                .map(ContactDto::of)
                .collect(Collectors.toSet());
        return new UserDto(user.getId(), user.getEmail(), user.getFullName(), user.getUsername(), UserRoleDto.of(user.getUserRole()), user.getBirthDay(), contacts);
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFullName() {
        return fullName;
    }

    public String getUsername() {
        return username;
    }

    public UserRoleDto getUserRole() {
        return userRole;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public Set<ContactDto> getContacts() {
        return contacts;
    }
}
