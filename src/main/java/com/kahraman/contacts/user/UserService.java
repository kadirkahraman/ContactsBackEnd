package com.kahraman.contacts.user;

import com.google.common.annotations.VisibleForTesting;
import com.kahraman.contacts.contact.Contact;
import com.kahraman.contacts.exceptions.ServiceException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository repository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public Optional<User> findByEmail(String email) {
        return repository.findByEmail(email);
    }

    public UserDto create(User user) {
        if (user.getId() != null) {
            throw new ServiceException("User id must be null on create process");
        }
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        logger.info("User {} is ready to create", user.getEmail());
        return save(user);
    }

    public UserDto update(User user) {
        if (user.getId() == null) {
            throw new ServiceException("User id is required parameter for update process");
        }
        if (StringUtils.isEmpty(user.getPassword())) {
            User dbUser = repository.findById(user.getId()).orElseThrow(() -> new ServiceException("User with id %d couldn't found", user.getId()));
            user.setPassword(dbUser.getPassword());
        } else {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        }
        logger.info("User {} (#{}) is ready to update", user.getEmail(), user.getId());
        return save(user);
    }

    @VisibleForTesting
    UserDto save(User user) {
        Optional<User> foundUser = repository.findByUsername(user.getUsername());
        if (foundUser.isPresent() && !foundUser.get().getId().equals(user.getId())) {
            throw new ServiceException("Username %s is already in use", user.getUsername());
        }
        user = repository.save(user);
        logger.info("User {} (#{}) is saved", user.getEmail(), user.getId());
        return UserDto.of(user);
    }

    public Set<UserDto> allUsers() {
        return repository.findAll()
                .stream()
                .map(UserDto::of)
                .collect(Collectors.toSet());
    }

    public Long delete(Long id) {
        Optional<User> user = repository.findById(id);
        if (!user.isPresent()) {
            throw new ServiceException("User with id %d couldn't found!", id);
        }
        return delete(user.get());
    }

    private Long delete(User user) {
        repository.delete(user);
        logger.info("User {} (#{}) is deleted", user.getEmail(), user.getId());
        return user.getId();
    }

    public UserDto updatePassword(User currentUser, String oldPass, String newPass) {
        if (!bCryptPasswordEncoder.matches(oldPass, currentUser.getPassword())) {
            throw new ServiceException("Old password couldn't verified");
        }
        currentUser.setPassword(bCryptPasswordEncoder.encode(newPass));
        return save(currentUser);
    }

    public long userCount() {
        return repository.count();
    }

    public void addContact(User user, Contact savedContact) {
        if (user.getContacts().stream().noneMatch(contact -> contact.getId().equals(savedContact.getId()))) {
            user.getContacts().add(savedContact);
            repository.save(user);
        }
    }

    public void removeContact(User user, Contact savedContact) {
        user.setContacts(user.getContacts().stream().filter(contact -> !contact.getId().equals(savedContact.getId())).collect(Collectors.toSet()));
        repository.save(user);
    }
}
