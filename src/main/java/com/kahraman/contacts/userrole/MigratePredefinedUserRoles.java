package com.kahraman.contacts.userrole;

import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import liquibase.exception.DatabaseException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MigratePredefinedUserRoles implements CustomTaskChange {

    private static final String INSERT_PERMISSION = "INSERT INTO user_role_permissions (id,permission) values (?,?)";
    private static final String INSERT_ROLE = "INSERT INTO user_role(id,description,name,protected_role) values (?,?,?,?)";
    private static final String SELECT_ROLE_ID = "SELECT id FROM user_role WHERE name = ?";

    private long hibernateSequence = 1;

    @Override
    public void execute(Database database) throws CustomChangeException {
        JdbcConnection connection = (JdbcConnection) database.getConnection();

        try {
            PreparedStatement preparedStatementRole = connection.prepareStatement(INSERT_ROLE);
            //Create user roles...
            for (PredefinedRole predefinedRole : PredefinedRole.values()) {
                UserRole role = new UserRoleBuilder()
                        .name(predefinedRole.getName())
                        .description(predefinedRole.getDescription())
                        .permissions(predefinedRole.getPermissions())
                        .protectedRole()
                        .build();

                preparedStatementRole.setLong(1, hibernateSequence++);
                preparedStatementRole.setString(2, role.getDescription());
                preparedStatementRole.setString(3, role.getName());
                preparedStatementRole.setBoolean(4, role.isProtectedRole());
                preparedStatementRole.addBatch();
            }
            preparedStatementRole.executeBatch();

            //Create permissions
            for (PredefinedRole predefinedRole : PredefinedRole.values()) {
                PreparedStatement preparedStatementForSelectId = connection.prepareStatement(SELECT_ROLE_ID);
                preparedStatementForSelectId.setString(1, predefinedRole.getName());
                ResultSet resultSet = preparedStatementForSelectId.executeQuery();
                if (resultSet.next()) {
                    long roleId = resultSet.getLong(1);
                    PreparedStatement preparedStatementForPermission = connection.prepareStatement(INSERT_PERMISSION);
                    for (Permission permission : predefinedRole.getPermissions()) {
                        preparedStatementForPermission.setLong(1, roleId);
                        preparedStatementForPermission.setString(2, permission.name());
                        preparedStatementForPermission.addBatch();
                    }
                    preparedStatementForPermission.executeBatch();
                }
            }

            //Create hibernate Sequence
            connection
                    .prepareStatement(String.format("INSERT INTO hibernate_sequence (next_val) VALUES (%d)", hibernateSequence))
                    .execute();
        } catch (DatabaseException | SQLException e) {
            throw new CustomChangeException("Predefined user role migration failed!", e);
        }
    }

    @Override
    public String getConfirmationMessage() {
        return "Predefined roles will migrating...";
    }

    @Override
    public void setUp() throws SetupException {
    }

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {

    }

    @Override
    public ValidationErrors validate(Database database) {
        return null;
    }
}
