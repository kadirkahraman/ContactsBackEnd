package com.kahraman.contacts.userrole;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kahraman.contacts.enums.SerializableEnum;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Permission implements SerializableEnum {

    ROLE_SYSTEM_ADMIN("System Admin", "System administrator privilege. This privilege allows manage all user accounts."),
    ROLE_USER_ADMIN("User Admin", "User management privilege. This privilege allows create/delete new users and list all user accounts"),
    ROLE_USER_VIEW("User View", "User view privilege. This privilege allows only listing all user accounts."),
    ROLE_CONTACT_ADMIN("Contact Admin", "Contact management privilege. This allows contact updates, edit or delete processes.");

    private final String name;
    private final String visibleName;

    Permission(String name, String visibleName) {
        this.name = name;
        this.visibleName = visibleName;
    }

    public String getName() {
        return name;
    }

    public String getVisibleName() {
        return visibleName;
    }

    @Override
    public String getEnumName() {
        return name();
    }
}
