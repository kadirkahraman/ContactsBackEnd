package com.kahraman.contacts.userrole;

import static com.kahraman.contacts.userrole.Permission.*;

public enum PredefinedRole {

    SYSTEM_ADMIN("System Administrator", "Manage all system and user accounts", Permission.values()),
    STANDARD_USER("Standard User", "Standard system users role", ROLE_CONTACT_ADMIN, ROLE_USER_ADMIN, ROLE_USER_VIEW),
    GUEST("Guest", "Guest accounts' role", ROLE_USER_VIEW);

    private final String name;
    private final String description;
    private final Permission[] permissions;

    PredefinedRole(String name, String description, Permission... permissions) {
        this.name = name;
        this.description = description;
        this.permissions = permissions;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Permission[] getPermissions() {
        return permissions;
    }
}
