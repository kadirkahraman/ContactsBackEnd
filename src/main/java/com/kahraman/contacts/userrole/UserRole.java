package com.kahraman.contacts.userrole;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "user_role")
public class UserRole {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "contactSequenceGenerator")
    @GenericGenerator(name = "contactSequenceGenerator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = "sequence_name", value = "HIBERNATE_SEQUENCE")
            })
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "user_role_permissions", joinColumns = @JoinColumn(name = "id"))
    @Column(name = "permission")
    @Enumerated(value = EnumType.STRING)
    private Set<Permission> permissions;

    @Column(name = "protected_role")
    private boolean protectedRole;

    UserRole() {
    }

    public Long getId() {
        return id;
    }

    void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    void setDescription(String description) {
        this.description = description;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    public boolean isProtectedRole() {
        return protectedRole;
    }

    void setProtectedRole(boolean protectedRole) {
        this.protectedRole = protectedRole;
    }
}
