package com.kahraman.contacts.userrole;

import com.kahraman.contacts.exceptions.BuilderException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class UserRoleBuilder {

    private UserRole userRole;

    public UserRoleBuilder() {
        this.userRole = new UserRole();
        this.userRole.setProtectedRole(false);
    }

    public UserRoleBuilder id(Long id) {
        userRole.setId(id);
        return this;
    }

    public UserRoleBuilder name(String name) {
        userRole.setName(name);
        return this;
    }

    public UserRoleBuilder description(String description) {
        userRole.setDescription(description);
        return this;
    }

    public UserRoleBuilder permissions(Set<Permission> permissions) {
        userRole.setPermissions(permissions);
        return this;
    }

    public UserRoleBuilder permissions(Permission... permissions) {
        return permissions(new HashSet<>(Arrays.asList(permissions)));
    }

    UserRoleBuilder protectedRole() {
        userRole.setProtectedRole(true);
        return this;
    }

    public UserRole build() {
        if (StringUtils.isEmpty(userRole.getName())) {
            throw new BuilderException("Role name couldn't be empty.");
        }

        if (StringUtils.isEmpty(userRole.getDescription())) {
            throw new BuilderException("Role description couldn't be empty.");
        }

        if (CollectionUtils.isEmpty(userRole.getPermissions())) {
            throw new BuilderException("Role must has min 1 permission.");
        }

        if (userRole.isProtectedRole()) {
            boolean foundPredefinedRule = Stream.of(PredefinedRole.values()).anyMatch(predefinedRole -> predefinedRole.getName().equals(userRole.getName()));
            if (!foundPredefinedRule) {
                throw new BuilderException("Protected roles can be only predefined roles.");
            }
            if (userRole.getId() != null) {
                throw new BuilderException("Protected roles can't be editable or deletable.");
            }
        }
        return this.userRole;
    }
}
