package com.kahraman.contacts.userrole;

import org.springframework.lang.Nullable;

import java.util.Set;

public class UserRoleDto {

    private final Long id;
    private final String name;
    private final String description;
    private final Set<Permission> permissions;
    private final boolean protectedRole;

    private UserRoleDto(Long id, String name, String description, Set<Permission> permissions, boolean protectedRole) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.permissions = permissions;
        this.protectedRole = protectedRole;
    }

    public static UserRoleDto of(@Nullable UserRole userRole) {
        if (userRole == null) {
            return null;
        }
        return new UserRoleDto(userRole.getId(), userRole.getName(), userRole.getDescription(), userRole.getPermissions(), userRole.isProtectedRole());
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public boolean isProtectedRole() {
        return protectedRole;
    }
}
