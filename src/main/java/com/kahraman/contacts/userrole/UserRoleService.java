package com.kahraman.contacts.userrole;

import com.google.common.annotations.VisibleForTesting;
import com.kahraman.contacts.exceptions.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserRoleService {
    private static final Logger logger = LoggerFactory.getLogger(UserRoleService.class);

    @Autowired
    private UserRoleRepository userRoleRepository;

    public UserRoleDto create(UserRole userRole) {
        if (userRole.getId() != null) {
            throw new ServiceException("User role id must be null on create process");
        }
        logger.info("User role with name {} preparing to create", userRole.getName());
        return save(userRole);
    }

    public UserRoleDto update(UserRole userRole) {
        if (userRole.getId() == null) {
            throw new ServiceException("User role id required for update process");
        }
        logger.info("User role {}#{} preparing to create", userRole.getName(), userRole.getId());
        return save(userRole);
    }

    @VisibleForTesting
    UserRoleDto save(UserRole userRole) {
        Optional<UserRole> checkRoleOnDb = findUserRoleByName(userRole.getName());
        if (checkRoleOnDb.isPresent() && !checkRoleOnDb.get().getId().equals(userRole.getId())) {
            logger.error("User role with name {} is already exist. Role name is using by {} role", userRole.getName(), userRole.getId());
            throw new ServiceException("User role with name %s is already exist!", userRole.getName());
        }
        UserRole savedRole = userRoleRepository.save(userRole);
        logger.info("User role with name {} was created", userRole.getName());
        return UserRoleDto.of(savedRole);
    }

    public Long delete(UserRole userRole) {
        return delete(userRole.getId());
    }

    public Long delete(Long id) {
        Optional<UserRole> checkRoleOnDb = userRoleRepository.findById(id);
        if (!checkRoleOnDb.isPresent()) {
            throw new ServiceException("User role with id #%d couldn't found. Delete process canceled", id);
        }
        userRoleRepository.deleteById(id);
        logger.info("User role {}#{} was deleted", checkRoleOnDb.get().getName(), checkRoleOnDb.get().getId());
        return id;
    }

    public List<UserRoleDto> list() {
        return userRoleRepository.findAll()
                .stream()
                .map(UserRoleDto::of)
                .collect(Collectors.toList());
    }

    public Optional<UserRole> findUserRoleByName(String name) {
        return userRoleRepository.findByName(name);
    }
}
