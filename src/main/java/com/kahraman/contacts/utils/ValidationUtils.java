package com.kahraman.contacts.utils;

import java.util.regex.Pattern;

public class ValidationUtils {

    /**
     * Validation pattern is same with Android Email Pattern
     *
     * @param emailAddress email address that will be control.
     * @return if email address is valid return true, otherwise false.
     */
    public static boolean validateMail(String emailAddress) {
        if (emailAddress == null || emailAddress.isEmpty()) {
            return false;
        }

        if (!emailAddress.contains("@")) {
            return false;
        }

        if (emailAddress.endsWith(".")) {
            return false;
        }

        Pattern EMAIL_ADDRESS = Pattern.compile(
                "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}"
                        + "\\@"
                        + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}"
                        + "("
                        + "\\."
                        + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}"
                        + ")+"
        );

        return EMAIL_ADDRESS.matcher(emailAddress).matches();
    }

    /**
     * Validation pattern is same with Android Phone Pattern
     *
     * @param phoneNumber phone number that will be control.
     * @return if phone number is valid return true, otherwise false.
     */
    public static boolean validatePhone(String phoneNumber) {
        if (phoneNumber == null || phoneNumber.isEmpty()) {
            return false;
        }

        Pattern PHONE = Pattern.compile(
                "(\\+[0-9]+[\\- \\.]*)?"
                        + "(\\([0-9]+\\)[\\- \\.]*)?"
                        + "([0-9][0-9\\- \\.]+[0-9])");

        return PHONE.matcher(phoneNumber).matches();
    }
}
