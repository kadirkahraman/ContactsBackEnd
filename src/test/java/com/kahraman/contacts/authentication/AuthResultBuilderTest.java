package com.kahraman.contacts.authentication;

import com.kahraman.contacts.exceptions.BuilderException;
import com.kahraman.contacts.user.User;
import com.kahraman.contacts.user.UserDto;
import org.junit.Test;

import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class AuthResultBuilderTest {

    @Test
    public void testBuild() {
        UserDto authenticatedUser = mock(UserDto.class);
        String token = "1234567890";
        JwtTokenUtil tokenUtil = mock(JwtTokenUtil.class);
        long expireDate = 1576087248000L;
        String username = "testUser";

        doReturn(new Date(expireDate)).when(tokenUtil).getExpirationDateFromToken(token);
        doReturn(username).when(tokenUtil).getUsernameFromToken(token);
        doReturn(false).when(tokenUtil).isTokenExpired(token);

        AuthResult authResult = new AuthResultBuilder(authenticatedUser)
                .token(token)
                .tokenDetails(tokenUtil)
                .build();

        assertThat(authResult.getToken(), is(token));
        assertThat(authResult.getAuthenticatedUser(), is(authenticatedUser));
        assertThat(authResult.getTokenDetails().getExpireDate(), is(expireDate));
        assertThat(authResult.getTokenDetails().getUsername(), is(username));
        assertThat(authResult.getTokenDetails().isTokenExpired(), is(false));
    }

    @Test(expected = BuilderException.class)
    public void testBuildWithOutToken() {
        new AuthResultBuilder(mock(UserDto.class)).build();
    }

    @Test(expected = BuilderException.class)
    public void testBuildWithNullUser() {
        new AuthResultBuilder((User) null)
                .token("1234567890")
                .tokenDetails(mock(JwtTokenUtil.class))
                .build();
    }

    @Test
    public void testBuildWithNullTokenDetails() {
        String token = "1234567890";
        UserDto authenticatedUser = mock(UserDto.class);

        new AuthResultBuilder(authenticatedUser)
                .token(token)
                .build();
    }
}
