package com.kahraman.contacts.authentication;

import com.kahraman.contacts.springsecurity.JwtUserDetailsService;
import com.kahraman.contacts.springsecurity.SecurityPrincipal;
import com.kahraman.contacts.user.User;
import com.kahraman.contacts.user.UserBuilder;
import com.kahraman.contacts.userrole.UserRole;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AuthenticationService.class)
public class AuthenticationServiceTest {

    @Autowired
    private AuthenticationService authenticationService;

    @MockBean
    private AuthenticationManager authenticationManager;
    @MockBean
    private JwtUserDetailsService userDetailsService;
    @MockBean
    private JwtTokenUtil jwtTokenUtil;

    @Before
    public void setUp() {
        doReturn(mock(Authentication.class)).when(authenticationManager).authenticate(any());
    }

    @Test
    public void testLoginAndGenerateToken() {
        SecurityPrincipal mockUserDetails = mock(SecurityPrincipal.class);
        doReturn(mockUserDetails).when(userDetailsService).loadUserByUsername(eq("testUser"));
        doReturn("123456789").when(jwtTokenUtil).generateToken(eq(mockUserDetails));
        User user = new UserBuilder()
                .id(1L)
                .email("test@test.test")
                .fullName("John")
                .username("john")
                .userRole(mock(UserRole.class))
                .password("123")
                .build();
        doReturn(user).when(mockUserDetails).getUser();

        AuthResult authResult = authenticationService.loginAndGenerateToken("testUser", "pass");
        assertThat(authResult.getToken(), CoreMatchers.is("123456789"));
    }
}
