package com.kahraman.contacts.authentication;

import io.jsonwebtoken.Claims;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = JwtTokenUtil.class)
public class JwtTokenUtilTest {

    @Autowired
    private JwtTokenUtil tokenUtil;

    @Mock
    private UserDetails userDetails;

    private String token;
    private String tokenWithClaims;

    @Before
    public void setUp() {
        doReturn("testUser").when(userDetails).getUsername();
        token = tokenUtil.generateToken(userDetails);
        Map<String, Object> claims = new HashMap<>();
        claims.put("claim1", "Claim 1 Value");
        claims.put("claim2", "Claim 2 Value");
        tokenWithClaims = tokenUtil.generateToken(claims, userDetails);
    }

    @Test
    public void testGetUsernameFromToken() {
        String username = tokenUtil.getUsernameFromToken(token);
        assertThat(username, is("testUser"));
    }

    @Test
    public void testGetExpirationDateFromToken() {
        Date expireDate = tokenUtil.getExpirationDateFromToken(token);
        assertThat(expireDate.getTime(), greaterThan(new Date().getTime()));
    }

    @Test
    public void testGetClaimFromToken() {
        Object foundClaim = tokenUtil.getClaimFromToken(tokenWithClaims, claims -> claims.get("claim2"));
        assertNotNull(foundClaim);
        String claimValue = String.valueOf(foundClaim);
        assertThat(StringUtils.isEmpty(claimValue), is(false));
        assertThat(claimValue, is("Claim 2 Value"));
    }

    @Test
    public void testGetAllClaimsFromToken() {
        Claims claims = tokenUtil.getAllClaimsFromToken(tokenWithClaims);
        assertThat(claims.size(), greaterThan(2));
        Set<String> allKeys=claims.keySet();
        assertThat(allKeys.contains("claim1"),is(true));
        assertThat(allKeys.contains("claim2"),is(true));
        assertThat(allKeys.contains("claimTest"),is(false));
    }

    @Test
    public void testIsTokenExpired() {
        Boolean isExpired = tokenUtil.isTokenExpired(token);
        assertThat(isExpired, is(false));
    }

    @Test
    public void testValidateToken() {
        boolean validationResult = tokenUtil.validateToken(token, userDetails);
        assertThat(validationResult, is(true));
    }

    @Test
    public void testValidateTokenFail() {
        UserDetails details = mock(UserDetails.class);
        doReturn("userTest").when(details).getUsername();
        boolean validationResult = tokenUtil.validateToken(token, details);
        assertThat(validationResult, is(false));
    }

    @Test
    public void testCompleteToken() {
        String tokenWithPrefix = tokenUtil.completeToken(token);
        assertThat(tokenWithPrefix, is(String.format("%s %s", "Basic", token)));
    }

}
