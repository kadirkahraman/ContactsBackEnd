package com.kahraman.contacts.contact;

import com.kahraman.contacts.exceptions.BuilderException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ContactBuilderTest {

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testEmptyFName() {
        expectedEx.expect(BuilderException.class);
        expectedEx.expectMessage("Contact first name couldn't be empty.");

        new ContactBuilder()
                .withCity("Ankara")
                .build();
    }

    @Test
    public void testEmptyLName() {
        expectedEx.expect(BuilderException.class);
        expectedEx.expectMessage("Contact last name couldn't be empty.");

        new ContactBuilder()
                .withFirstName("Test User")
                .withCity("Ankara")
                .build();
    }

    @Test
    public void testEmptyPhone() {
        expectedEx.expect(BuilderException.class);
        expectedEx.expectMessage("Contact phone couldn't be empty.");

        new ContactBuilder()
                .withFirstName("FName")
                .withLastName("LName")
                .withCity("Ankara")
                .build();
    }

    @Test
    public void testBuilder() {
        Long id = 1L;
        String firstName = "FName";
        String lastName = "LName";
        String nickName = "Nick";
        String city = "Bursa";
        Date birthDay = new Date();
        String phone = "3123 55";
        String phoneAlternative = "3123 54";
        String mail = "test@test.test";
        String mailAlternative = "test2@test.test";
        Boolean favourite = false;

        Contact contact = new ContactBuilder()
                .withId(id)
                .withFirstName(firstName)
                .withLastName(lastName)
                .withNickName(nickName)
                .withCity(city)
                .withBirthDay(birthDay)
                .withPhone(phone)
                .withPhoneAlternative(phoneAlternative)
                .withMail(mail)
                .withMailAlternative(mailAlternative)
                .withFavourite(favourite)
                .build();

        assertThat(contact.getId(), is(id));
        assertThat(contact.getFirstName(), is(firstName));
        assertThat(contact.getLastName(), is(lastName));
        assertThat(contact.getNickName(), is(nickName));
        assertThat(contact.getCity(), is(city));
        assertThat(contact.getBirthDay(), is(birthDay));
        assertThat(contact.getPhone(), is(phone));
        assertThat(contact.getPhoneAlternative(), is(phoneAlternative));
        assertThat(contact.getMail(), is(mail));
        assertThat(contact.getMailAlternative(), is(mailAlternative));
        assertThat(contact.getFavourite(), is(favourite));
    }
}
