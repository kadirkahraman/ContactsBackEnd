package com.kahraman.contacts.contact;

import org.junit.Test;

import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNull;

public class ContactDtoTest {

    @Test
    public void testWithNullContact() {
        ContactDto dto = ContactDto.of(null);
        assertNull(dto);
    }

    @Test
    public void testContactDto() {
        Long id = 1L;
        String firstName = "FName";
        String lastName = "LName";
        String nickName = "Nick";
        String city = "Bursa";
        Date birthDay = new Date();
        String phone = "3123 55";
        String phoneAlternative = "3123 54";
        String mail = "test@test.test";
        String mailAlternative = "test2@test.test";
        Boolean favourite = false;

        Contact contact = new ContactBuilder()
                .withId(id)
                .withFirstName(firstName)
                .withLastName(lastName)
                .withNickName(nickName)
                .withCity(city)
                .withBirthDay(birthDay)
                .withPhone(phone)
                .withPhoneAlternative(phoneAlternative)
                .withMail(mail)
                .withMailAlternative(mailAlternative)
                .withFavourite(favourite)
                .build();

        ContactDto dto = ContactDto.of(contact);
        assertThat(dto.getId(), is(id));
        assertThat(dto.getFirstName(), is(firstName));
        assertThat(dto.getLastName(), is(lastName));
        assertThat(dto.getNickName(), is(nickName));
        assertThat(dto.getCity(), is(city));
        assertThat(dto.getBirthDay(), is(birthDay));
        assertThat(dto.getPhone(), is(phone));
        assertThat(dto.getPhoneAlternative(), is(phoneAlternative));
        assertThat(dto.getMail(), is(mail));
        assertThat(dto.getMailAlternative(), is(mailAlternative));
        assertThat(dto.getFavourite(), is(favourite));
    }
}
