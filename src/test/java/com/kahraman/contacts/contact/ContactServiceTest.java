package com.kahraman.contacts.contact;

import com.kahraman.contacts.authentication.AuthenticationService;
import com.kahraman.contacts.exceptions.ServiceException;
import com.kahraman.contacts.user.User;
import com.kahraman.contacts.user.UserBuilder;
import com.kahraman.contacts.user.UserService;
import com.kahraman.contacts.userrole.UserRole;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ContactService.class)
public class ContactServiceTest {

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();
    @Autowired
    private ContactService contactService;
    @MockBean
    private ContactRepository contactRepository;
    @MockBean
    private AuthenticationService authenticationService;
    @MockBean
    private UserService userService;

    private User authenticatedUser;

    @Before
    public void setUp() {
        authenticatedUser = new UserBuilder()
                .email("asd@zas.asd")
                .userRole(mock(UserRole.class))
                .fullName("Test User")
                .username("user")
                .password("123")
                .birthDay(new Date())
                .build();
        doReturn(Optional.of(authenticatedUser)).when(authenticationService).authenticatedUser();
    }

    @Test
    public void testCreate() {
        Contact contact = createEntity(null);
        doReturn(contact).when(contactRepository).save(contact);
        ContactDto dto = contactService.create(contact);
        assertThat(dto.getPhone(), is("555"));
        verify(userService, times(1)).addContact(authenticatedUser, contact);
    }

    @Test
    public void verifyCreateError() {
        expectedEx.expect(ServiceException.class);
        expectedEx.expectMessage("Contact id must be null on create process");

        Contact contact = createEntity(3L);
        contactService.create(contact);
    }

    @Test
    public void testUpdate() {
        Contact contact = createEntity(1L);
        doReturn(contact).when(contactRepository).save(contact);
        ContactDto dto = contactService.update(contact);
        assertThat(dto.getPhone(), is("555"));
        assertThat(dto.getId(), is(1L));
        verify(userService, times(1)).addContact(authenticatedUser, contact);
    }

    @Test
    public void verifyUpdateError() {
        expectedEx.expect(ServiceException.class);
        expectedEx.expectMessage("contact id required for update process");

        Contact contact = createEntity(null);
        contactService.update(contact);
    }

    @Test
    public void testDelete() {
        Contact contact = createEntity(10L);
        doReturn(Optional.of(contact)).when(contactRepository).findById(contact.getId());
        long deletedId = contactService.delete(contact.getId());
        assertThat(deletedId, is(10L));
        verify(userService, times(1)).removeContact(authenticatedUser, contact);
    }

    @Test
    public void verifyDeleteError() {
        expectedEx.expect(ServiceException.class);
        expectedEx.expectMessage(String.format("Contact with id %s couldn't found", 5L));

        doReturn(Optional.empty()).when(contactRepository).findById(5L);
        contactService.delete(5L);
    }

    private Contact createEntity(Long id) {
        return new ContactBuilder()
                .withFirstName("FName")
                .withLastName("LName")
                .withPhone("555")
                .withId(id)
                .build();
    }
}
