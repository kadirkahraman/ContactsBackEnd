package com.kahraman.contacts.converters;

import com.kahraman.contacts.userrole.Permission;
import com.kahraman.contacts.userrole.UserRole;
import com.kahraman.contacts.userrole.UserRoleBuilder;
import org.springframework.core.convert.converter.Converter;

import javax.annotation.Nonnull;

public class UserRoleConverter implements Converter<String, UserRole> {
    @Override
    public UserRole convert(@Nonnull String source) {
        if ("1".equals(source)) {
            return new UserRoleBuilder()
                    .id(1L)
                    .name("Admin Role")
                    .description("Role Description")
                    .permissions(Permission.values())
                    .build();
        }
        return null;
    }
}