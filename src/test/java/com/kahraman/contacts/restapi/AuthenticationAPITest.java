package com.kahraman.contacts.restapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kahraman.contacts.authentication.AuthResult;
import com.kahraman.contacts.authentication.AuthResultBuilder;
import com.kahraman.contacts.authentication.AuthenticationService;
import com.kahraman.contacts.authentication.JwtTokenUtil;
import com.kahraman.contacts.converters.UserRoleConverter;
import com.kahraman.contacts.user.User;
import com.kahraman.contacts.user.UserBuilder;
import com.kahraman.contacts.user.UserDto;
import com.kahraman.contacts.user.UserService;
import com.kahraman.contacts.userrole.UserRole;
import com.kahraman.contacts.userrole.UserRoleService;
import net.javacrumbs.jsonunit.JsonAssert;
import net.javacrumbs.jsonunit.core.Option;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Date;
import java.util.Optional;

import static com.kahraman.contacts.userrole.PredefinedRole.STANDARD_USER;
import static com.kahraman.contacts.userrole.PredefinedRole.SYSTEM_ADMIN;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest(classes = AuthenticationAPI.class)
@RunWith(SpringRunner.class)
public class AuthenticationAPITest {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Autowired
    private AuthenticationAPI api;

    @MockBean
    private JwtTokenUtil tokenUtil;
    @MockBean
    private AuthenticationService authenticationService;
    @MockBean
    private UserService userService;
    @MockBean
    private UserRoleService userRoleService;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        FormattingConversionService formattingConversionService = new FormattingConversionService();
        formattingConversionService.addConverter(new UserRoleConverter());
        this.mockMvc = MockMvcBuilders.standaloneSetup(api)
                .setConversionService(formattingConversionService)
                .build();
    }

    @Test
    public void testLogin() throws Exception {
        long expireDate = 1576087248000L;
        doReturn("test@test.test").when(tokenUtil).getUsernameFromToken(anyString());
        doReturn(new Date(expireDate)).when(tokenUtil).getExpirationDateFromToken(anyString());
        doReturn(false).when(tokenUtil).isTokenExpired(anyString());

        User user = new UserBuilder()
                .id(1L)
                .email("test@test.test")
                .fullName("John")
                .username("john")
                .userRole(mock(UserRole.class))
                .password("123")
                .build();
        AuthResult authResult = new AuthResultBuilder(UserDto.of(user))
                .token("token")
                .tokenDetails(tokenUtil)
                .build();

        LoginModel loginModel = new LoginModel();
        loginModel.setUsername("test@test.test");
        loginModel.setPassword("testPassword");
        doReturn(authResult).when(authenticationService).loginAndGenerateToken(loginModel.getUsername(), loginModel.getPassword());
        ResultActions resultActions = mockMvc.perform(
                post("/api/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(OBJECT_MAPPER.writeValueAsString(loginModel))
        );
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        assertThat(response.getStatus(), is(200));
        JsonAssert.assertJsonEquals("{\"authenticatedUser\":{\"id\":1,\"email\":\"test@test.test\",\"fullName\":\"John\",\"username\":\"john\",\"userRole\":{\"id\":0,\"name\":null,\"description\":null,\"permissions\":[],\"protectedRole\":false},\"birthDay\":null,\"contacts\":[]},\"token\":\"token\",\"tokenDetails\":{\"username\":\"test@test.test\",\"expireDate\":1576087248000,\"tokenExpired\":false}}", response.getContentAsString(), JsonAssert.when(Option.IGNORING_ARRAY_ORDER));
    }

    @Test
    public void testSignUpFirstUser() throws Exception {
        doReturn(Optional.of(mock(UserRole.class))).when(userRoleService).findUserRoleByName(STANDARD_USER.getName());
        doReturn(Optional.of(mock(UserRole.class))).when(userRoleService).findUserRoleByName(SYSTEM_ADMIN.getName());
        doReturn(0L).when(userService).userCount();

        ResultActions resultActions = mockMvc.perform(
                post("/api/auth/signUp")
                        .param("email", "test@test.test")
                        .param("fullName", "John Doe")
                        .param("username", "john")
                        .param("userRole", "1")
                        .param("password", "123456")
        );
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        assertThat(response.getStatus(), is(200));
        verify(userRoleService).findUserRoleByName(SYSTEM_ADMIN.getName());
        JsonAssert.assertJsonEquals("", response.getContentAsString(), JsonAssert.when(Option.IGNORING_ARRAY_ORDER));
    }

    @Test
    public void testSignUp() throws Exception {
        doReturn(Optional.of(mock(UserRole.class))).when(userRoleService).findUserRoleByName(STANDARD_USER.getName());
        doReturn(1L).when(userService).userCount();
        ResultActions resultActions = mockMvc.perform(
                post("/api/auth/signUp")
                        .param("email", "test@test.test")
                        .param("fullName", "John Doe")
                        .param("username", "john")
                        .param("userRole", "1")
                        .param("password", "123456")
        );
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        assertThat(response.getStatus(), is(200));
        verify(userRoleService, never()).findUserRoleByName(SYSTEM_ADMIN.getName());
        JsonAssert.assertJsonEquals("", response.getContentAsString(), JsonAssert.when(Option.IGNORING_ARRAY_ORDER));
    }

    @Test
    public void testUpdatePassword() throws Exception {
        User mockUser = mock(User.class);
        doReturn(Optional.of(mockUser)).when(authenticationService).authenticatedUser();

        ResultActions resultActions = mockMvc.perform(
                post("/api/auth/updatePassword")
                        .param("oldPass", "123")
                        .param("newPass", "321")
        );
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        verify(userService).updatePassword(mockUser, "123", "321");
        assertThat(response.getStatus(), is(200));
        JsonAssert.assertJsonEquals("", response.getContentAsString(), JsonAssert.when(Option.IGNORING_ARRAY_ORDER));
    }
}
