package com.kahraman.contacts.restapi;

import com.kahraman.contacts.contact.ContactBuilder;
import com.kahraman.contacts.contact.ContactDto;
import com.kahraman.contacts.contact.ContactService;
import net.javacrumbs.jsonunit.JsonAssert;
import net.javacrumbs.jsonunit.core.Option;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest(classes = ContactAPI.class)
@RunWith(SpringRunner.class)
public class ContactAPITest {

    @Autowired
    private ContactAPI api;

    @MockBean
    private ContactService contactService;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(api).build();
    }

    @Test
    public void testCreate() throws Exception {
        ContactDto dto = ContactDto.of(new ContactBuilder().withFirstName("FName").withLastName("LName").withId(1L).withPhone("555").build());
        doReturn(dto).when(contactService).create(argThat(argument -> "555".equals(argument.getPhone()) && "FName".equals(argument.getFirstName()) && "LName".equals(argument.getLastName())));
        ResultActions resultActions = mockMvc.perform(
                post("/api/contact")
                        .param("firstName", "FName")
                        .param("lastName", "LName")
                        .param("phone", "555")
        );
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        assertThat(response.getStatus(), is(200));
        JsonAssert.assertJsonEquals("{\"id\":1,\"firstName\":\"FName\",\"lastName\":\"LName\",\"nickName\":null,\"city\":null,\"birthDay\":null,\"phone\":\"555\",\"phoneAlternative\":null,\"mail\":null,\"mailAlternative\":null,\"favourite\":null}", response.getContentAsString(), JsonAssert.when(Option.IGNORING_ARRAY_ORDER));
    }

    @Test
    public void testUpdate() throws Exception {
        ContactDto dto = ContactDto.of(new ContactBuilder().withFirstName("FName").withLastName("LName").withId(1L).withPhone("555").build());
        doReturn(dto).when(contactService).update(argThat(argument -> argument.getId().equals(1L) && "555".equals(argument.getPhone()) && "FName".equals(argument.getFirstName()) && "LName".equals(argument.getLastName())));
        ResultActions resultActions = mockMvc.perform(
                put("/api/contact")
                        .param("firstName", "FName")
                        .param("lastName", "LName")
                        .param("phone", "555")
                        .param("id", "1")
        );
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        assertThat(response.getStatus(), is(200));
        JsonAssert.assertJsonEquals("{\"id\":1,\"firstName\":\"FName\",\"lastName\":\"LName\",\"nickName\":null,\"city\":null,\"birthDay\":null,\"phone\":\"555\",\"phoneAlternative\":null,\"mail\":null,\"mailAlternative\":null,\"favourite\":null}", response.getContentAsString(), JsonAssert.when(Option.IGNORING_ARRAY_ORDER));
    }

    @Test
    public void testDelete() throws Exception {
        doReturn(1L).when(contactService).delete(1L);
        ResultActions resultActions = mockMvc.perform(delete("/api/contact/1"));
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        assertThat(response.getStatus(), is(200));
        assertThat(response.getContentAsString(), is("1"));
    }
}
