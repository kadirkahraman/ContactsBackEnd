package com.kahraman.contacts.restapi;

import com.google.common.collect.ImmutableSet;
import com.kahraman.contacts.converters.UserRoleConverter;
import com.kahraman.contacts.user.UserBuilder;
import com.kahraman.contacts.user.UserDto;
import com.kahraman.contacts.user.UserService;
import com.kahraman.contacts.userrole.UserRole;
import com.kahraman.contacts.userrole.UserRoleBuilder;
import net.javacrumbs.jsonunit.JsonAssert;
import net.javacrumbs.jsonunit.core.Option;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Set;

import static com.kahraman.contacts.userrole.Permission.ROLE_USER_ADMIN;
import static com.kahraman.contacts.userrole.Permission.ROLE_USER_VIEW;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;


@SpringBootTest(classes = UserAPI.class)
@RunWith(SpringRunner.class)
public class UserAPITest {
    @Autowired
    private UserAPI api;

    @MockBean
    private UserService userService;

    private MockMvc mockMvc;
    private UserRole role;

    @Before
    public void setUp() {
        FormattingConversionService formattingConversionService = new FormattingConversionService();
        formattingConversionService.addConverter(new UserRoleConverter());
        this.mockMvc = MockMvcBuilders.standaloneSetup(api)
                .setConversionService(formattingConversionService)
                .build();
        role = new UserRoleBuilder()
                .id(1L)
                .description("Description")
                .name("Role")
                .permissions(ROLE_USER_ADMIN, ROLE_USER_VIEW)
                .build();
    }

    @Test
    public void testList() throws Exception {
        Set<UserDto> list = ImmutableSet.of(
                UserDto.of(new UserBuilder().id(1L).password("123").userRole(role).email("test@test.test").username("user").fullName("Test").build()),
                UserDto.of(new UserBuilder().id(2L).password("123").userRole(role).email("test@test.test").username("user").fullName("Test").build())
        );
        doReturn(list).when(userService).allUsers();
        ResultActions resultActions = mockMvc.perform(get("/api/user"));
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        assertThat(response.getStatus(), is(200));
        JsonAssert.assertJsonEquals("[{\"id\":1,\"email\":\"test@test.test\",\"fullName\":\"Test\",\"username\":\"user\",\"userRole\":{\"id\":1,\"name\":\"Role\",\"description\":\"Description\",\"permissions\":[{\"name\":\"User View\",\"visibleName\":\"User view privilege. This privilege allows only listing all user accounts.\",\"enumName\":\"ROLE_USER_VIEW\"},{\"name\":\"User Admin\",\"visibleName\":\"User management privilege. This privilege allows create/delete new users and list all user accounts\",\"enumName\":\"ROLE_USER_ADMIN\"}],\"protectedRole\":false},\"birthDay\":null,\"contacts\":[]},{\"id\":2,\"email\":\"test@test.test\",\"fullName\":\"Test\",\"username\":\"user\",\"userRole\":{\"id\":1,\"name\":\"Role\",\"description\":\"Description\",\"permissions\":[{\"name\":\"User View\",\"visibleName\":\"User view privilege. This privilege allows only listing all user accounts.\",\"enumName\":\"ROLE_USER_VIEW\"},{\"name\":\"User Admin\",\"visibleName\":\"User management privilege. This privilege allows create/delete new users and list all user accounts\",\"enumName\":\"ROLE_USER_ADMIN\"}],\"protectedRole\":false},\"birthDay\":null,\"contacts\":[]}]", response.getContentAsString(), JsonAssert.when(Option.IGNORING_ARRAY_ORDER));
    }

    @Test
    public void testCreate() throws Exception {
        UserDto dto = UserDto.of(new UserBuilder().id(175L).password("123").userRole(role).email("test@test.test").username("user").fullName("Test").build());
        doReturn(dto).when(userService).create(argThat(argument -> "Test".equals(argument.getFullName()) && "user".equals(argument.getUsername()) && "test@test.test".equals(argument.getEmail())));
        ResultActions resultActions = mockMvc.perform(
                post("/api/user")
                        .param("fullName", "Test")
                        .param("username", "user")
                        .param("email", "test@test.test")
                        .param("userRole", "1")
                        .param("password", "1")
                        .param("birthDay", "25/11/2002")
        );
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        assertThat(response.getStatus(), is(200));
        JsonAssert.assertJsonEquals("{\"id\":175,\"email\":\"test@test.test\",\"fullName\":\"Test\",\"username\":\"user\",\"userRole\":{\"id\":1,\"name\":\"Role\",\"description\":\"Description\",\"permissions\":[{\"name\":\"User View\",\"visibleName\":\"User view privilege. This privilege allows only listing all user accounts.\",\"enumName\":\"ROLE_USER_VIEW\"},{\"name\":\"User Admin\",\"visibleName\":\"User management privilege. This privilege allows create/delete new users and list all user accounts\",\"enumName\":\"ROLE_USER_ADMIN\"}],\"protectedRole\":false},\"birthDay\":null,\"contacts\":[]}", response.getContentAsString(), JsonAssert.when(Option.IGNORING_ARRAY_ORDER));
    }

    @Test
    public void testUpdate() throws Exception {
        UserDto dto = UserDto.of(new UserBuilder().password("123").id(1L).userRole(role).email("test@test.test").username("user").fullName("Test").build());
        doReturn(dto).when(userService).update(argThat(argument -> "Test".equals(argument.getFullName()) && "user".equals(argument.getUsername()) && "test@test.test".equals(argument.getEmail())));
        ResultActions resultActions = mockMvc.perform(
                put("/api/user")
                        .param("fullName", "Test")
                        .param("username", "user")
                        .param("email", "test@test.test")
                        .param("userRole", "1")
                        .param("password", "1")
                        .param("birthDay", "25/11/2002")
        );
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        assertThat(response.getStatus(), is(200));
        JsonAssert.assertJsonEquals("{\"id\":1,\"email\":\"test@test.test\",\"fullName\":\"Test\",\"username\":\"user\",\"userRole\":{\"id\":1,\"name\":\"Role\",\"description\":\"Description\",\"permissions\":[{\"name\":\"User View\",\"visibleName\":\"User view privilege. This privilege allows only listing all user accounts.\",\"enumName\":\"ROLE_USER_VIEW\"},{\"name\":\"User Admin\",\"visibleName\":\"User management privilege. This privilege allows create/delete new users and list all user accounts\",\"enumName\":\"ROLE_USER_ADMIN\"}],\"protectedRole\":false},\"birthDay\":null,\"contacts\":[]}", response.getContentAsString(), JsonAssert.when(Option.IGNORING_ARRAY_ORDER));
    }

    @Test
    public void testDelete() throws Exception {
        doReturn(1L).when(userService).delete(1L);
        ResultActions resultActions = mockMvc.perform(delete("/api/user/1"));
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        assertThat(response.getStatus(), is(200));
        assertThat(response.getContentAsString(), is("1"));
    }
}
