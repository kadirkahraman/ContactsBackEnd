package com.kahraman.contacts.restapi;

import com.kahraman.contacts.userrole.UserRoleBuilder;
import com.kahraman.contacts.userrole.UserRoleDto;
import com.kahraman.contacts.userrole.UserRoleService;
import net.javacrumbs.jsonunit.JsonAssert;
import net.javacrumbs.jsonunit.core.Option;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static com.kahraman.contacts.userrole.Permission.ROLE_USER_ADMIN;
import static com.kahraman.contacts.userrole.Permission.ROLE_USER_VIEW;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest(classes = UserRoleAPI.class)
@RunWith(SpringRunner.class)
public class UserRoleAPITest {

    @Autowired
    private UserRoleAPI api;

    @MockBean
    private UserRoleService userRoleService;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(api).build();
    }

    @Test
    public void testPermissions() throws Exception {
        ResultActions resultActions = mockMvc.perform(get("/api/userrole/permissions"));
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        assertThat(response.getStatus(), is(200));
        JsonAssert.assertJsonEquals("[{\"name\":\"System Admin\",\"visibleName\":\"System administrator privilege. This privilege allows manage all user accounts.\",\"enumName\":\"ROLE_SYSTEM_ADMIN\"},{\"name\":\"User Admin\",\"visibleName\":\"User management privilege. This privilege allows create/delete new users and list all user accounts\",\"enumName\":\"ROLE_USER_ADMIN\"},{\"name\":\"User View\",\"visibleName\":\"User view privilege. This privilege allows only listing all user accounts.\",\"enumName\":\"ROLE_USER_VIEW\"},{\"name\":\"Contact Admin\",\"visibleName\":\"Contact management privilege. This allows contact updates, edit or delete processes.\",\"enumName\":\"ROLE_CONTACT_ADMIN\"}]", response.getContentAsString(), JsonAssert.when(Option.IGNORING_ARRAY_ORDER));
    }

    @Test
    public void testList() throws Exception {
        List<UserRoleDto> list = Arrays.asList(
                UserRoleDto.of(new UserRoleBuilder().id(1L).permissions(ROLE_USER_ADMIN, ROLE_USER_VIEW).description("desc1").name("name1").build()),
                UserRoleDto.of(new UserRoleBuilder().id(2L).permissions(ROLE_USER_VIEW).description("desc2").name("name2").build())
        );
        doReturn(list).when(userRoleService).list();
        ResultActions resultActions = mockMvc.perform(get("/api/userrole"));
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        assertThat(response.getStatus(), is(200));
        JsonAssert.assertJsonEquals("[{\"id\":1,\"name\":\"name1\",\"description\":\"desc1\",\"permissions\":[{\"name\":\"User Admin\",\"visibleName\":\"User management privilege. This privilege allows create/delete new users and list all user accounts\",\"enumName\":\"ROLE_USER_ADMIN\"},{\"name\":\"User View\",\"visibleName\":\"User view privilege. This privilege allows only listing all user accounts.\",\"enumName\":\"ROLE_USER_VIEW\"}],\"protectedRole\":false},{\"id\":2,\"name\":\"name2\",\"description\":\"desc2\",\"permissions\":[{\"name\":\"User View\",\"visibleName\":\"User view privilege. This privilege allows only listing all user accounts.\",\"enumName\":\"ROLE_USER_VIEW\"}],\"protectedRole\":false}]", response.getContentAsString(), JsonAssert.when(Option.IGNORING_ARRAY_ORDER));
    }

    @Test
    public void testCreate() throws Exception {
        UserRoleDto dto = UserRoleDto.of(new UserRoleBuilder().name("name-1").description("description-1").id(1L).permissions(ROLE_USER_ADMIN).build());
        doReturn(dto).when(userRoleService).create(argThat(argument -> "name-1".equals(argument.getName()) && "description-1".equals(argument.getDescription()) && argument.getPermissions().size() == 1 && argument.getPermissions().contains(ROLE_USER_ADMIN)));
        ResultActions resultActions = mockMvc.perform(
                post("/api/userrole")
                        .param("name", "name-1")
                        .param("description", "description-1")
                        .param("permissions", ROLE_USER_ADMIN.name())
        );
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        assertThat(response.getStatus(), is(200));
        JsonAssert.assertJsonEquals("{\"id\":1,\"name\":\"name-1\",\"description\":\"description-1\",\"permissions\":[{\"name\":\"User Admin\",\"visibleName\":\"User management privilege. This privilege allows create/delete new users and list all user accounts\",\"enumName\":\"ROLE_USER_ADMIN\"}],\"protectedRole\":false}", response.getContentAsString(), JsonAssert.when(Option.IGNORING_ARRAY_ORDER));
    }

    @Test
    public void testUpdate() throws Exception {
        UserRoleDto dto = UserRoleDto.of(new UserRoleBuilder().name("name-1").description("description-1").id(1L).permissions(ROLE_USER_ADMIN).build());
        doReturn(dto).when(userRoleService).update(argThat(argument -> argument.getId().equals(1L) && "name-1".equals(argument.getName()) && "description-1".equals(argument.getDescription()) && argument.getPermissions().size() == 1 && argument.getPermissions().contains(ROLE_USER_ADMIN)));
        ResultActions resultActions = mockMvc.perform(
                put("/api/userrole")
                        .param("name", "name-1")
                        .param("description", "description-1")
                        .param("id", "1")
                        .param("permissions", ROLE_USER_ADMIN.name())
        );
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        assertThat(response.getStatus(), is(200));
        JsonAssert.assertJsonEquals("{\"id\":1,\"name\":\"name-1\",\"description\":\"description-1\",\"permissions\":[{\"name\":\"User Admin\",\"visibleName\":\"User management privilege. This privilege allows create/delete new users and list all user accounts\",\"enumName\":\"ROLE_USER_ADMIN\"}],\"protectedRole\":false}", response.getContentAsString(), JsonAssert.when(Option.IGNORING_ARRAY_ORDER));
    }

    @Test
    public void testDelete() throws Exception {
        doReturn(1L).when(userRoleService).delete(1L);
        ResultActions resultActions = mockMvc.perform(delete("/api/userrole/1"));
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        assertThat(response.getStatus(), is(200));
        assertThat(response.getContentAsString(), is("1"));
    }

}
