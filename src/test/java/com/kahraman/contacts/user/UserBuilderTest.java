package com.kahraman.contacts.user;

import com.kahraman.contacts.contact.Contact;
import com.kahraman.contacts.exceptions.BuilderException;
import com.kahraman.contacts.userrole.UserRole;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;

public class UserBuilderTest {

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void verifyMailError() {
        expectedEx.expect(BuilderException.class);
        expectedEx.expectMessage("Email format is not valid");

        new UserBuilder()
                .build();
    }

    @Test
    public void verifyNameError() {
        expectedEx.expect(BuilderException.class);
        expectedEx.expectMessage("Name is required parameter");

        new UserBuilder()
                .email("test@test.test")
                .build();
    }

    @Test
    public void verifyUsernameError() {
        expectedEx.expect(BuilderException.class);
        expectedEx.expectMessage("Username is required parameter");

        new UserBuilder()
                .email("test@test.test")
                .fullName("John")
                .build();
    }

    @Test
    public void verifyUserRoleError() {
        expectedEx.expect(BuilderException.class);
        expectedEx.expectMessage("User role is required parameter");

        new UserBuilder()
                .email("test@test.test")
                .fullName("John")
                .username("john")
                .build();
    }

    @Test
    public void verifyPasswordError() {
        expectedEx.expect(BuilderException.class);
        expectedEx.expectMessage("Password is required parameter");

        new UserBuilder()
                .email("test@test.test")
                .fullName("John")
                .username("john")
                .userRole(mock(UserRole.class))
                .build();
    }

    @Test
    public void testBuild() {
        Set<Contact> contactSet = new HashSet<>();
        contactSet.add(mock(Contact.class));
        contactSet.add(mock(Contact.class));
        User user = new UserBuilder()
                .id(1L)
                .email("test@test.test")
                .fullName("John")
                .username("john")
                .userRole(mock(UserRole.class))
                .password("123")
                .birthDay(new Date(1576087248000L))
                .contacts(contactSet)
                .addContacts(mock(Contact.class))
                .build();

        assertThat(user.getId(), is(1L));
        assertThat(user.getEmail(), is("test@test.test"));
        assertThat(user.getFullName(), is("John"));
        assertThat(user.getUsername(), is("john"));
        assertThat(user.getPassword(), is("123"));
        assertThat(user.getBirthDay(), is(new Date(1576087248000L)));
        assertThat(user.getContacts().size(), is(3));
    }
}
