package com.kahraman.contacts.user;

import com.kahraman.contacts.contact.Contact;
import com.kahraman.contacts.userrole.UserRole;
import org.junit.Test;

import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;

public class UserDtoTest {

    @Test
    public void testWithNull() {
        UserDto userDto = UserDto.of(null);
        assertNull(userDto);
    }

    @Test
    public void testWithUser() {
        Contact mockContact = mock(Contact.class);
        User user = new UserBuilder()
                .id(1L)
                .email("test@test.test")
                .fullName("John")
                .username("john")
                .userRole(mock(UserRole.class))
                .password("123")
                .birthDay(new Date(1576087248000L))
                .addContacts(mockContact)
                .build();
        UserDto dto = UserDto.of(user);

        assertThat(dto.getId(), is(user.getId()));
        assertThat(dto.getEmail(), is(user.getEmail()));
        assertThat(dto.getFullName(), is(user.getFullName()));
        assertThat(dto.getUsername(), is(user.getUsername()));
        assertThat(dto.getBirthDay(), is(user.getBirthDay()));
        assertThat(dto.getContacts().size(), is(user.getContacts().size()));
    }
}
