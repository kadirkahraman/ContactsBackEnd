package com.kahraman.contacts.user;

import com.kahraman.contacts.authentication.AuthenticationService;
import com.kahraman.contacts.userrole.Permission;
import com.kahraman.contacts.userrole.UserRole;
import com.kahraman.contacts.userrole.UserRoleBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@DataJpaTest(showSql = false)
@ActiveProfiles("test")
@ContextConfiguration(classes = {
        UserRepositoryTest.TestPersistenceContextConfig.class,
        UserRepositoryTest.TestBeanConfig.class
})
@MockBean(classes = {AuthenticationService.class, BCryptPasswordEncoder.class})
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TestEntityManager em;

    private UserRole userRole;

    @Before
    public void setUp() {
        userRole = em.persist(
                new UserRoleBuilder()
                        .description("Test description 1")
                        .name("Test role 1")
                        .permissions(Permission.ROLE_CONTACT_ADMIN, Permission.ROLE_USER_VIEW, Permission.ROLE_USER_ADMIN)
                        .build()
        );

        userRepository.save(createUser("test@test.test", "john"));
        userRepository.save(createUser("test2@test.test", "john2"));
    }

    @Test
    public void testFindByEmail() {
        Optional<User> user = userRepository.findByEmail("test@test.test");
        assertThat(user.isPresent(), is(true));
        assertThat(user.get().getUsername(), is("john"));

        Optional<User> invalidUser = userRepository.findByEmail("testinvaliduser@test.test");
        assertFalse(invalidUser.isPresent());
    }

    @Test
    public void testFindAll() {
        List<User> users = userRepository.findAll();
        assertThat(users.size(), is(2));
    }

    @Test
    public void testFindByUsername() {
        Optional<User> user = userRepository.findByUsername("john2");
        assertThat(user.isPresent(), is(true));
        assertThat(user.get().getUsername(), is("john2"));

        Optional<User> invalidUser = userRepository.findByUsername("invalidusername");
        assertFalse(invalidUser.isPresent());
    }

    private User createUser(String email, String username) {
        return new UserBuilder()
                .email(email)
                .fullName("John")
                .username(username)
                .userRole(userRole)
                .password("123")
                .birthDay(new Date(1576087248000L))
                .contacts(null)
                .build();
    }

    @Configuration
    @EnableTransactionManagement
    @EnableJpaRepositories(basePackages = {"com.kahraman.contacts.user"})
    @EntityScan(basePackages = {"com.kahraman.contacts.user","com.kahraman.contacts.userrole", "com.kahraman.contacts.contact"})
    static class TestPersistenceContextConfig {
    }

    @Configuration
    @ComponentScan(basePackages = {"com.kahraman.contacts.user"})
    static class TestBeanConfig {
    }
}
