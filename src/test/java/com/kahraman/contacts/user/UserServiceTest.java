package com.kahraman.contacts.user;

import com.kahraman.contacts.contact.Contact;
import com.kahraman.contacts.exceptions.ServiceException;
import com.kahraman.contacts.userrole.UserRole;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UserService.class)
public class UserServiceTest {

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;
    @MockBean
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Mock
    private Contact mockContact;

    @Test
    public void verifyCreateError() {
        expectedEx.expect(ServiceException.class);
        expectedEx.expectMessage("User id must be null on create process");

        User user = createUser(1L);
        userService.create(user);
    }

    @Test
    public void verifySaveError() {
        expectedEx.expect(ServiceException.class);
        expectedEx.expectMessage("Username john is already in use");

        User user = createUser(1L);
        doReturn(Optional.of(createUser(5L))).when(userRepository).findByUsername("john");
        userService.save(user);
    }

    @Test
    public void testCreate() {
        User user = createUser(null);
        doReturn(createUser(1L)).when(userRepository).save(user);
        UserDto dto = userService.create(user);
        assertThat(dto.getId(), is(1L));
        assertThat(dto.getUsername(), is("john"));
    }

    @Test
    public void verifyUpdateError() {
        expectedEx.expect(ServiceException.class);
        expectedEx.expectMessage("User id is required parameter for update process");

        User user = createUser(null);
        userService.update(user);
    }

    @Test
    public void testUpdate() {
        User user = createUser(1L);
        doReturn(user).when(userRepository).save(user);
        UserDto dto = userService.update(user);
        assertThat(dto.getId(), is(1L));
        assertThat(dto.getUsername(), is("john"));
    }

    @Test
    public void verifyDeleteError() {
        expectedEx.expect(ServiceException.class);
        expectedEx.expectMessage("User with id 5 couldn't found!");

        doReturn(Optional.empty()).when(userRepository).findById(5L);
        userService.delete(5L);
    }

    @Test
    public void testDelete() {
        User mockUser = mock(User.class);
        doReturn(5L).when(mockUser).getId();
        doReturn(Optional.of(mockUser)).when(userRepository).findById(5L);

        Long deletedId = userService.delete(5L);
        assertThat(deletedId, is(5L));
    }

    @Test
    public void verifyUpdatePasswordError() {
        expectedEx.expect(ServiceException.class);
        expectedEx.expectMessage("Old password couldn't verified");

        User user = mock(User.class);
        doReturn("encodedPass").when(user).getPassword();
        doReturn(false).when(bCryptPasswordEncoder).matches("oldPass", "encodedPass");
        userService.updatePassword(user, "oldPass", "newPass");
    }

    private User createUser(Long id) {
        return new UserBuilder()
                .id(id)
                .email("test@test.test")
                .fullName("John")
                .username("john")
                .userRole(mock(UserRole.class))
                .password("123")
                .birthDay(new Date(1576087248000L))
                .contacts(Collections.emptySet())
                .addContacts(mockContact)
                .build();
    }
}
