package com.kahraman.contacts.userrole;

import com.kahraman.contacts.userrole.Permission;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class PermissionTest {

    @Test
    public void verifyPermissionsStartsWithROLE_() {
        for (Permission permission : Permission.values()) {
            assertThat(permission.name().startsWith("ROLE_"), is(true));
        }
    }

    @Test
    public void verifyPermissionNamesDifference() {
        Set<String> permissionNames = Stream.of(Permission.values())
                .map(Permission::getName)
                .collect(Collectors.toSet());
        assertThat(permissionNames.size(), is(Permission.values().length));
    }

    @Test
    public void verifyVisibleNamesNotEmpty() {
        List<Permission> filteredPermissions = Stream.of(Permission.values())
                .filter(permission -> StringUtils.isEmpty(permission.getVisibleName()))
                .collect(Collectors.toList());
        String permissionNames = filteredPermissions
                .stream()
                .map(Permission::name)
                .collect(Collectors.joining(", "));
        assertThat(String.format("%s Permissions have empty permission descriptions. Please add visible values for these", permissionNames), filteredPermissions.size(), is(0));
    }
}
