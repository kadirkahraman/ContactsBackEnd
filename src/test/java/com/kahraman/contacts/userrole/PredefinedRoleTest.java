package com.kahraman.contacts.userrole;

import com.kahraman.contacts.userrole.PredefinedRole;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class PredefinedRoleTest {

    private Stream<PredefinedRole> predefinedRoleStream;

    @Before
    public void setUp() {
        predefinedRoleStream = Stream.of(PredefinedRole.values());
    }

    @Test
    public void verifyHasRoleNames() {
        predefinedUserRoleValueCheck(PredefinedRole::getName, "Role Names");
    }

    @Test
    public void testRoleNamesUniqueness() {
        Set<String> roleNames = predefinedRoleStream.map(PredefinedRole::getName)
                .collect(Collectors.toSet());
        assertThat("Duplicate role names detected", roleNames.size(), is(PredefinedRole.values().length));
    }

    @Test
    public void verifyRolePermissions() {
        List<PredefinedRole> filteredRoles = predefinedRoleStream
                .filter(predefinedRole -> ArrayUtils.isEmpty(predefinedRole.getPermissions()))
                .collect(Collectors.toList());
        String filteredRoleNames = filteredRoles.stream()
                .map(PredefinedRole::name)
                .collect(Collectors.joining(", "));
        String failMessage = String.format("%s predefined user roles has empty permissions. Please repair these.", filteredRoleNames);
        assertThat(failMessage, filteredRoles.size(), is(0));
    }

    @Test
    public void verifyHasDescription() {
        predefinedUserRoleValueCheck(PredefinedRole::getDescription, "Descriptions");
    }

    private void predefinedUserRoleValueCheck(Function<PredefinedRole, String> filterFunction, String filterName) {
        List<PredefinedRole> filteredRoles = predefinedRoleStream
                .filter(predefinedRole -> StringUtils.isEmpty(filterFunction.apply(predefinedRole)))
                .collect(Collectors.toList());
        String filteredRoleNames = filteredRoles.stream()
                .map(PredefinedRole::name)
                .collect(Collectors.joining(", "));
        String failMessage = String.format("%s predefined user roles has empty %s. Please repair these.", filteredRoleNames, filterName);
        assertThat(failMessage, filteredRoles.size(), is(0));
    }
}
