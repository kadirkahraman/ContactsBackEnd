package com.kahraman.contacts.userrole;

import com.kahraman.contacts.exceptions.BuilderException;
import com.kahraman.contacts.userrole.PredefinedRole;
import com.kahraman.contacts.userrole.UserRole;
import com.kahraman.contacts.userrole.UserRoleBuilder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static com.kahraman.contacts.userrole.Permission.ROLE_CONTACT_ADMIN;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNull;

public class UserRoleBuilderTest {

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void validateRoleHasName() {
        expectedEx.expect(BuilderException.class);
        expectedEx.expectMessage("Role name couldn't be empty.");

        new UserRoleBuilder()
                .id(15L)
                .description("anythings")
                .permissions(ROLE_CONTACT_ADMIN)
                .build();


    }

    @Test
    public void validateRoleHasDescription() {
        expectedEx.expect(BuilderException.class);
        expectedEx.expectMessage("Role description couldn't be empty.");

        new UserRoleBuilder()
                .id(15L)
                .name("anythings")
                .permissions(ROLE_CONTACT_ADMIN)
                .build();
    }

    @Test
    public void validateMin1Permission() {
        expectedEx.expect(BuilderException.class);
        expectedEx.expectMessage("Role must has min 1 permission.");

        new UserRoleBuilder()
                .id(15L)
                .name("anythings")
                .description("anythings")
                .build();
    }

    @Test
    public void testProtectedIsTrueOnlyPredefinedRoles() {
        expectedEx.expect(BuilderException.class);
        expectedEx.expectMessage("Protected roles can be only predefined roles.");

        new UserRoleBuilder()
                .id(15L)
                .name("anythings")
                .description("anythings")
                .permissions(ROLE_CONTACT_ADMIN)
                .protectedRole()
                .build();
    }


    @Test
    public void testUpdateProtectedRole() {
        expectedEx.expect(BuilderException.class);
        expectedEx.expectMessage("Protected roles can't be editable or deletable.");

        PredefinedRole predefinedRole = PredefinedRole.STANDARD_USER;
        new UserRoleBuilder()
                .name(predefinedRole.getName())
                .description(predefinedRole.getDescription())
                .permissions(predefinedRole.getPermissions())
                .protectedRole()
                .id(2L)
                .build();
    }

    @Test
    public void testBuild() {
        UserRole role = new UserRoleBuilder()
                .id(15L)
                .name("Role name")
                .description("Role desc")
                .permissions(ROLE_CONTACT_ADMIN)
                .build();

        assertThat(role.getId(), is(15L));
        assertThat(role.getName(), is("Role name"));
        assertThat(role.getDescription(), is("Role desc"));
        assertThat(role.getPermissions().size(), is(1));
        assertThat(role.getPermissions(), hasItem(ROLE_CONTACT_ADMIN));
        assertThat(role.isProtectedRole(), is(false));
    }

    @Test
    public void testProtectedBuild() {
        PredefinedRole predefinedRole = PredefinedRole.STANDARD_USER;
        UserRole role = new UserRoleBuilder()
                .name(predefinedRole.getName())
                .description(predefinedRole.getDescription())
                .permissions(predefinedRole.getPermissions())
                .protectedRole()
                .build();

        assertNull(role.getId());
        assertThat(role.getName(), is(predefinedRole.getName()));
        assertThat(role.getDescription(), is(predefinedRole.getDescription()));
        assertThat(role.getPermissions().size(), is(predefinedRole.getPermissions().length));
        assertThat(role.getPermissions(), hasItems(predefinedRole.getPermissions()));
        assertThat(role.isProtectedRole(), is(true));
    }

}
