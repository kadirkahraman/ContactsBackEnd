package com.kahraman.contacts.userrole;

import com.kahraman.contacts.userrole.Permission;
import com.kahraman.contacts.userrole.UserRole;
import com.kahraman.contacts.userrole.UserRoleBuilder;
import com.kahraman.contacts.userrole.UserRoleDto;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNull;

public class UserRoleDtoTest {

    @Test
    public void testWithNull() {
        UserRoleDto dto = UserRoleDto.of(null);
        assertNull(dto);
    }

    @Test
    public void testDto() {
        UserRole userRole = new UserRoleBuilder()
                .id(1L)
                .permissions(Permission.ROLE_CONTACT_ADMIN)
                .name("Test role")
                .description("Test Description")
                .build();
        UserRoleDto dto = UserRoleDto.of(userRole);

        assertThat(userRole.getId(), is(dto.getId()));
        assertThat(userRole.getPermissions().size(), is(dto.getPermissions().size()));
        assertThat(userRole.getDescription(),is(dto.getDescription()));
        assertThat(userRole.getName(),is(dto.getName()));
        assertThat(userRole.isProtectedRole(),is(dto.isProtectedRole()));
    }
}
