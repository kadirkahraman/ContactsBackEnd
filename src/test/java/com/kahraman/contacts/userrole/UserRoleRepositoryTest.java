package com.kahraman.contacts.userrole;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest(showSql = false)
@ActiveProfiles("test")
@ContextConfiguration(classes = {
        UserRoleRepositoryTest.TestPersistenceContextConfig.class,
        UserRoleRepositoryTest.TestBeanConfig.class
})
public class UserRoleRepositoryTest {

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Before
    public void setUp() {
        userRoleRepository.save(
                new UserRoleBuilder()
                        .description("Test description 1")
                        .name("Test role 1")
                        .permissions(Permission.ROLE_CONTACT_ADMIN)
                        .build()
        );
        userRoleRepository.save(
                new UserRoleBuilder()
                        .description("Test description 2")
                        .name("Test role 2")
                        .permissions(Permission.ROLE_USER_ADMIN)
                        .build()
        );
    }

    @Test
    public void testFindByName() {
        Optional<UserRole> findRole1 = userRoleRepository.findByName("Test role 1");
        assertThat(findRole1.isPresent(), is(true));
        assertThat(findRole1.get().getDescription(), is("Test description 1"));

        Optional<UserRole> findRole2 = userRoleRepository.findByName("Test role 2");
        assertThat(findRole2.isPresent(), is(true));
        assertThat(findRole2.get().getDescription(), is("Test description 2"));

        Optional<UserRole> unknownRole = userRoleRepository.findByName("Any Role");
        assertThat(unknownRole.isPresent(), is(false));
    }

    @Configuration
    @EnableTransactionManagement
    @EnableJpaRepositories(basePackages = {"com.kahraman.contacts.userrole"})
    @EntityScan(basePackages = {"com.kahraman.contacts.userrole"})
    static class TestPersistenceContextConfig {
    }

    @Configuration
    @ComponentScan(basePackages = {"com.kahraman.contacts.userrole"})
    static class TestBeanConfig {
    }
}
