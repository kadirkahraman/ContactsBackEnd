package com.kahraman.contacts.userrole;

import com.kahraman.contacts.exceptions.ServiceException;
import com.kahraman.contacts.userrole.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static com.kahraman.contacts.userrole.Permission.ROLE_CONTACT_ADMIN;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

@SpringBootTest(classes = {UserRoleService.class})
@RunWith(SpringRunner.class)
public class UserRoleServiceTest {

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();
    @Autowired
    private UserRoleService userRoleService;
    @MockBean
    private UserRoleRepository userRoleRepository;

    @Test
    public void testSaveValidations() {
        UserRole userRole = createUserRole(null);
        doReturn(Optional.of(mock(UserRole.class))).when(userRoleRepository).findByName(userRole.getName());

        expectedEx.expect(ServiceException.class);
        expectedEx.expectMessage(String.format("User role with name %s is already exist!", userRole.getName()));

        userRoleService.save(userRole);
    }

    @Test
    public void testSave() {
        UserRole userRole = createUserRole(1L);
        doReturn(Optional.of(userRole)).when(userRoleRepository).findByName(userRole.getName());
        doReturn(userRole).when(userRoleRepository).save(userRole);
        UserRoleDto dto = userRoleService.save(userRole);
        assertThat(dto.getId(), is(userRole.getId()));
    }

    @Test
    public void testCreateInvalid() {
        expectedEx.expect(ServiceException.class);
        expectedEx.expectMessage("User role id must be null on create process");

        UserRole userRole = createUserRole(1L);
        userRoleService.create(userRole);
    }

    @Test
    public void testCreate() {
        UserRole userRole = createUserRole(null);
        doReturn(createUserRole(1L)).when(userRoleRepository).save(userRole);
        UserRoleDto dto = userRoleService.create(userRole);
        assertThat(dto.getId(), is(1L));
    }

    @Test
    public void testUpdateInvalid() {
        expectedEx.expect(ServiceException.class);
        expectedEx.expectMessage("User role id required for update process");

        UserRole userRole = createUserRole(null);
        userRoleService.update(userRole);
    }

    @Test
    public void testUpdate() {
        UserRole userRole = createUserRole(1L);
        doReturn(userRole).when(userRoleRepository).save(userRole);
        UserRoleDto dto = userRoleService.update(userRole);
        assertThat(dto.getId(), is(1L));
    }

    @Test
    public void testDeleteInvalid() {
        long id = 1L;
        expectedEx.expect(ServiceException.class);
        expectedEx.expectMessage(String.format("User role with id #%d couldn't found. Delete process canceled", id));
        doReturn(Optional.empty()).when(userRoleRepository).findById(id);
        userRoleService.delete(id);
    }

    @Test
    public void testDelete() {
        long id = 1L;
        doReturn(Optional.of(mock(UserRole.class))).when(userRoleRepository).findById(id);
        Long result = userRoleService.delete(id);
        assertThat(result, is(id));
    }

    private UserRole createUserRole(Long id) {
        return new UserRoleBuilder()
                .description("Any description")
                .name("Any Role Name")
                .permissions(ROLE_CONTACT_ADMIN)
                .id(id)
                .build();
    }
}
