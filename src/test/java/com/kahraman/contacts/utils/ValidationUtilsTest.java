package com.kahraman.contacts.utils;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ValidationUtilsTest {

    @Test
    public void testValidMail() {
        boolean result1 = ValidationUtils.validateMail("test@test.test");
        boolean result2 = ValidationUtils.validateMail("test1223321@test.test.tr");
        boolean result3 = ValidationUtils.validateMail("test_test_test@test-some.test");
        boolean result4 = ValidationUtils.validateMail("test-1212_123@test.test");
        boolean result5 = ValidationUtils.validateMail("test.testss@test.test");

        assertTrue(result1);
        assertTrue(result2);
        assertTrue(result3);
        assertTrue(result4);
        assertTrue(result5);
    }

    @Test
    public void testInvalidMail() {
        boolean result1 = ValidationUtils.validateMail(null);
        boolean result2 = ValidationUtils.validateMail("");
        boolean result3 = ValidationUtils.validateMail("test_test_test");
        boolean result4 = ValidationUtils.validateMail("test-1212_123@test");
        boolean result5 = ValidationUtils.validateMail("@test.test");

        assertFalse(result1);
        assertFalse(result2);
        assertFalse(result3);
        assertFalse(result4);
        assertFalse(result5);
    }

    @Test
    public void testPhoneMail() {
        boolean result1 = ValidationUtils.validatePhone("+905555555555");
        boolean result2 = ValidationUtils.validatePhone("05555555555");
        boolean result3 = ValidationUtils.validatePhone("5555555555");
        boolean result4 = ValidationUtils.validatePhone("0555 555 55 55");
        boolean result5 = ValidationUtils.validatePhone("+90 555 555 55 55");

        assertTrue(result1);
        assertTrue(result2);
        assertTrue(result3);
        assertTrue(result4);
        assertTrue(result5);
    }

    @Test
    public void testInvalidPhone() {
        boolean result1 = ValidationUtils.validatePhone(null);
        boolean result2 = ValidationUtils.validatePhone("");
        boolean result3 = ValidationUtils.validatePhone("sadasd");
        boolean result4 = ValidationUtils.validatePhone("555asd");
        boolean result5 = ValidationUtils.validatePhone("+90 555 555 55 ss");

        assertFalse(result1);
        assertFalse(result2);
        assertFalse(result3);
        assertFalse(result4);
        assertFalse(result5);
    }
}
